# Low Cost Recipe Finder #

This is an application built by students attending Oregon State University

### What is this repository for? ###

* Supports user's ability to find low cost, tasty recipes
* Current version: 0.0.1

### How do I get set up? ###

1. Make sure you have [php](http://php.net/manual/en/install.php) 5.4+ installed globally on your system `php -v` 
2. Clone this repository `git clone https://bitbucket.org/lowcostrecipefinders/recipe-finder.git path/to/project`
3. cd into the directory `cd path/to/project`
4. Install [Composer](http://getcomposer.org) `php -r "readfile('https://getcomposer.org/installer');" | php`
5. Run `php composer.phar update`
6. Run tests with `php vendor/bin/phpunit test`
7. Run a local server with `php -S localhost:8000 index.php` and visit it at [http://localhost:8000](http://localhost:8000)

### Contribution guidelines ###

* Code will follow the style guidelines specified in the [PSR-1](http://www.php-fig.org/psr/psr-1/), [PSR-2](http://www.php-fig.org/psr/psr-2/), and [PSR-4](http://www.php-fig.org/psr/psr-4/) standards
* Class files will go under the `src/` directory, have the base namespace of `RecipeFinders`, with each sub namespace in its own directory (following the PSR-4 standard)
* Tests will go under the `test/` directory with the base namespace of `RecipeFinders\Test` with each sub namespace in its own directory (following the PSR-4 standard)
* Test classes should mimic the namespace of the class they are testing and have the suffix 'Test'.
* For example, given the class `RecipeFinders\Security\User`, it would be located in the file `src/Security/User.php`. The corresponding test class would be `RecipeFinders\Test\Security\UserTest` and would be located at `test/Security/UserTest.php`

### Note on autoloading ###
The application utilizes [PHP autoloading](http://php.net/manual/en/language.oop5.autoload.php).  This means that, so long as your class file name and directory structure follow the guidelines specified above, you do not need to use `require` or `require_once` in your code to load a class file.  PHP will know how to find the class file and will load it for you.


### Who do I talk to? ###

* Contact [Mark](mailto:wardlem@onid.oregonstate.edu) if you have any questions.