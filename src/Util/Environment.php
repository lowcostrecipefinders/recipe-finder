<?php

namespace RecipeFinders\Util;

use RecipeFinders\Http\Request;

/**
 * Class Environment
 *
 * The environment object holds environment configuration variables
 *
 * @package RecipeFinders\Util
 */
class Environment extends ParameterBag
{
    const LOCAL_ENV = 1;
    const PROD_ENV = 2;

    /** @type int $env Either LOCAL_ENV or PROD_ENV */
    protected $env;

    public function __construct(Request $request)
    {
        $this->env = $this->detectEnvironment($request);
    }

    /**
     * Returns whether or not we are in a local environment
     *
     * @return bool
     */
    public function isLocal()
    {
        return $this->env === self::LOCAL_ENV;
    }

    /**
     * Returns whether or not we are in a production environment
     *
     * @return bool
     */
    public function isProduction()
    {
        return $this->env === self::PROD_ENV;
    }

    /**
     * Sets the environment values
     *
     * @param array $values An associative array of values to be set in this environment
     *
     * @return void
     */
    public function setValues(array $values)
    {
        foreach($values as $key => $value) {
            $this->values[$key] = $value;
        }
    }


    /**
     * Returns a string representation of the current environment
     *
     * @return string
     */
    public function environmentName()
    {
        return $this->isLocal() ? 'local' : 'prod';
    }

    /**
     * Determines whether we are in a production or local environment
     *
     * @param Request $request
     *
     * @return int  The constant for the environment
     */
    protected function detectEnvironment(Request $request)
    {
        $address = $request->server()->get('REMOTE_ADDR');
        return $address === '127.0.0.1' ? self::LOCAL_ENV : self::PROD_ENV;
    }

}