<?php

namespace RecipeFinders\Util;

/**
 * Class ParameterBag
 *
 * A utility object that holds values
 *
 * @package RecipeFinders\Http
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class ParameterBag
{

    /** @type array */
    protected $values = array();

    /**
     * Constructor
     *
     * @param array $values The values to instantiate the bag
     */
    public function __construct(array $values = array())
    {
        $this->values = $values;
    }


    protected function set($key, $value)
    {
        if (is_string($key)) {
            $this->values[$key] = $value;
        }

        return $this;
    }

    /**
     * Gets a value from the parameter bag by key
     *
     * @param string $key
     *
     * @return mixed|null Null is returned if the key does not exist
     */
    public function get($key)
    {
        return isset($this->values[$key]) ? $this->values[$key] : null;
    }

    /**
     * Determines if the parameter bag has a given key
     *
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return $this->get($key) !== null;
    }


}