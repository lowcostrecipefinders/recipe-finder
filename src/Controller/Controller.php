<?php

namespace RecipeFinders\Controller;

use RecipeFinders\DependencyInjection\Container;
use RecipeFinders\DependencyInjection\ContainerAware;
use RecipeFinders\Http\Request;
use RecipeFinders\Http\Response;
use RecipeFinders\Http\Session;
use RecipeFinders\Routing\Router;
use RecipeFinders\Util\Environment;
use RecipeFinders\View\View;

/**
 * Class Controller
 *
 * A base controller with useful methods
 *
 * @package RecipeFinders\Controller
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
abstract class Controller implements ContainerAware
{
    /** @type Container $container */
    protected $container;

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Set the container in the controller
     *
     * @param Container $container The container object that will be injected
     *
     * @return void
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return Environment
     */
    protected function getEnvironment()
    {
        return $this->container->get('environment');
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->container->get('request');
    }

    /**
     * @return Session
     */
    protected function getSession()
    {
        return $this->container->get('session');
    }

    /**
     * @return Router
     */
    protected function getRouter()
    {
        return $this->container->get('router');
    }

    /**
     * @param $url
     * @return Response
     */
    protected function redirect($url)
    {
        return new Response('', Response::HTTP_TEMPORARY_REDIRECT, array('Location' => $url));
    }

    /**
     * Renders a template
     *
     * @param string $file The file path of the template to be rendered relative to the templates directory
     * @param array  $data An array of data for the template to use
     *
     * @return string
     */
    protected function render($file, array $data = array())
    {
        $file = $this->getEnvironment()->get('document_root') . "/templates/" . $file;
        $view = $this->getView();
        return $view->renderFile($file, $data, $this);
    }

    /**
     * @param $routeName
     * @param array $params
     * @return null|string
     */
    protected function getUrl($routeName, $params = array())
    {
        return $this->getRouter()->getUrlForRoute($routeName, $params);
    }

    /**
     * @return null|View
     */
    protected function getView()
    {
        return $this->container->get('view');
    }


}