<?php

namespace RecipeFinders\Controller;

use RecipeFinders\Http\Request;
use RecipeFinders\Http\Response;
use RecipeFinders\Model\RecipeFinderInterface;
use RecipeFinders\Model\RecipeRankerInterface;

class SearchController extends Controller
{

    /**
     * Creates the home page
     *
     * @return string
     */
    public function homeAction()
    {
        return $this->render("main/home.php");
    }

    /**
     * Returns a page of search results for recipes ranked by price of ingredients
     *
     * @param Request $request
     *
     * @return Response|string
     */
    public function searchAction(Request $request)
    {

        $query = $request->query()->get('q');

        if (!$query) {
            $this->getSession()->setFlash('search_error', 'You need to enter a search term');
            $this->redirect('home');
        }

        $recipeFinder = $this->getRecipeFinder();
        $recipeRanker = $this->getRecipeRanker();

        $recipes = is_null($recipeFinder) ? array() : $recipeFinder->findRecipes($query);

        $rankedRecipes = is_null($recipeRanker) ? array() : $recipeRanker->rankRecipes($recipes);

        return $this->render('main/search_results.php', array('recipes' => $rankedRecipes));
    }

    /**
     * @return RecipeFinderInterface
     */
    protected function getRecipeFinder()
    {
        return $this->container->get('recipe_finder');
    }

    /**
     * @return RecipeRankerInterface
     */
    protected function getRecipeRanker()
    {
        return $this->container->get("RecipeFinders\\RecipeRanker\\RecipeRanker");
    }

}