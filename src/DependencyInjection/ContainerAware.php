<?php

namespace RecipeFinders\DependencyInjection;

/**
 * Interface ContainerAware
 *
 * It used by the dependency injection container to know whether or not it should inject itself
 *
 * @package RecipeFinders\DependencyInjection
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface ContainerAware
{
    /**
     * Set the container in the object
     *
     * @param Container $container The container object that will be injected
     *
     * @return void
     */
    public function setContainer(Container $container);
}