<?php

namespace RecipeFinders\DependencyInjection;
use RecipeFinders\DependencyInjection\Exception\ClassDoesNotExist;
use RecipeFinders\DependencyInjection\Exception\ContainerMissingClass;
use RecipeFinders\DependencyInjection\Exception\FailureToCreateInstance;
use RecipeFinders\DependencyInjection\Exception\MethodDoesNotExist;
use RecipeFinders\DependencyInjection\Exception\MissingParameter;

/**
 * Class Container
 *
 * The container is responsible for holding references to shared instances of objects.
 * It is also responsible for resolving dependencies for method arguments.
 *
 * @package RecipeFinders\DependencyInjection
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Container
{
    /** @type array $shared Holds a map of class names to shared objects */
    protected $shared = array();

    /**
     * Constructor
     *
     * @param bool $setSelf Whether or not to set this instance in the container
     */
    public function __construct( $setSelf = true )
    {
        if ( $setSelf ) {
            $this->share( $this );
        }
    }

    /**
     * Creates an object of the specified type, resolving constructor parameters through reflection.
     *
     * This method will also inject this container if a class implements the ContainerAware interface
     *
     * @param string $className The name of the class to be created
     * @param array  $params    An associative array of specific parameters for the constructor
     *
     * @return object|null  Returns null if the class doesn't exist
     * @throws ClassDoesNotExist
     * @throws FailureToCreateInstance
     */
    public function createObject( $className, array $params = array() )
    {
        if (!class_exists( $className )) {
            throw new ClassDoesNotExist($className);
        }

        // Get arguments for constructor
        try {
            $arguments = $this->resolveMethodArguments( $className, '__construct', $params );
            // Get the reflection for the class
            $classReflection = new \ReflectionClass( $className );
            // Create a new instance
            $object = $classReflection->newInstanceArgs( $arguments );

            // Check if the container should be injected
            if ( $object instanceof ContainerAware ) {
                $object->setContainer( $this );
            }

            return $object;
        } catch (\Exception $e) {
            throw new FailureToCreateInstance($className, $e);
        }
    }

    /**
     * Calls a methods on an object, resolving method parameters through reflection.
     *
     * @param object $object The object whose method should be called
     * @param string $method The name of the method to be called for the object
     * @param array  $params An associative array of parameter names to values
     *
     * @return mixed Whatever the called method returns
     */
    public function callObjectMethod( $object, $method, array $params = array() )
    {
        // Get the arguments
        $arguments = $this->resolveMethodArguments( $object, $method, $params );

        $methodReflection = new \ReflectionMethod($object, $method);

        // Get access so we can fix it later
        $access = $methodReflection->isPublic() ? true : false;

        // Make accessible, even if private or protected
        $methodReflection->setAccessible(true);

        // Call it
        $result = $methodReflection->invokeArgs($object, $arguments );

        // Set accessibility back to the way it should be
        $methodReflection->setAccessible($access);

        return $result;
    }

    /**
     * Adds an object to the shared array which is used for resolving method dependencies
     *
     * @param object|\Closure $object    The object to be shared or a factory function that will
     *                                   create the object when it is first accessed
     * @param string          $alias     An alias to easily retrieve this object from the container
     *                                   If $object is a \Closure, this should be the class name for the
     *                                   shared object that the function returns.
     * @param string          $altAlias  An alias to easily retrieve this object from the container
     *                                   It is only used when $object is a \Closure
     * @param
     */
    public function share( $object, $alias = null, $altAlias = null )
    {
        $key = get_class($object);
        if ($object instanceof \Closure) {
            $key = $alias;
            $alias = $altAlias;
        }

        if (!$key) {
            // TODO throw exception??
            return;
        }

        $this->shared[$key] = $object;
        if ( $alias != null ) {
            $this->shared[$alias] = $key;
        }
    }

    /**
     * Creates an alias in the container
     *
     * Technically, the alias can be created for any type of value, except
     * that a string value will be interpreted as being an alias itself,
     * which will create another look up and never return the string.
     *
     * Normally, the $refersTo value would be the name of a class that already
     * exists in the container or an alias that already exists.  This is especially
     * useful for setting a default interface implementation for dependency injection.
     *
     * Alternatively, the $refersTo value can be an object. This is useful when a
     * an object of the same type already exists in the container and you need a
     * different shared object for that class (for example, if you needed connections
     * to two separate databases).  Note though that only the primary implementation
     * will be used for injecting dependencies into methods.
     *
     * @param string        $alias    The alias is a key that will be used to retrieve
     *                                the $refersTo value from the container
     * @param string|object $refersTo This is a value that the alias refers to
     *                                See the method description for more details
     */
    public function addAlias($alias, $refersTo)
    {
        $this->shared[$alias] = $refersTo;
    }


    /**
     * Returns a value from the shared container
     *
     * @param string $key The class name or alias of
     *
     * @return object|null The object stored in the container under the key, or null if the key does not exist
     */
    public function get( $key )
    {
        if (!isset($this->shared[$key])) {
            return null;
        }

        $value = $this->shared[$key];

        // Redirect for aliases
        if (is_string($value)) {
            return $this->get($value);
        }

        // If this is a factory function, trigger it and save it for later
        if ($value instanceof \Closure) {
            $this->shared[$key] = $value();
            $value = $this->shared[$key];
        }

        return $value;
    }

    /**
     * Determines if the container has a value
     *
     * @param string $key The class name or alias used to lookup the value
     *
     * @return bool
     */
    public function has( $key )
    {
        return $this->get($key) !== null;
    }

    /**
     * Resolves the arguments for a method call
     *
     * @param string|object $object The class name or an instance of the class
     * @param string        $method The name of the method for the arguments to be resolved for
     * @param array         $params Associative array of specific parameters used for argument resolution
     *
     * @return array        The arguments for the method in a simple (numeric) array
     * @throws MethodDoesNotExist
     * @throws ContainerMissingClass
     * @throws MissingParameter
     */
    public function resolveMethodArguments( $object, $method, array $params = array() )
    {
        if (!method_exists($object, $method)) {
            $className = is_object($object) ? get_class($object) : $object;
            throw new MethodDoesNotExist($className, $method);
        }

        /** @type array $arguments The arguments that will be returned */
        $arguments = array();

        $methodReflection = new \ReflectionMethod($object, $method);
        $reflectionParams = $methodReflection->getParameters();

        /** @type \ReflectionParameter $reflectionParam */
        foreach($reflectionParams as $reflectionParam) {
            // If the params array has a value for this argument, we use it
            if (isset($params[$reflectionParam->name])) {
                $arguments[] = $params[$reflectionParam->name];
            }
            // Otherwise, we use a default if it is available
            elseif ($reflectionParam->isDefaultValueAvailable()) {
                $arguments[] = $reflectionParam->getDefaultValue();
            }
            // Lastly, we inject a dependency from the container if it exists
            elseif (!$reflectionParam->allowsNull()) {
                $paramReflectionClass = $reflectionParam->getClass();
                if (!$this->has($paramReflectionClass->name)) {
                    $className = is_object($object) ? get_class($object) : $object;
                    throw new ContainerMissingClass($className, $method, $reflectionParam->name);
                }
                $arguments[] = $this->get($paramReflectionClass->name);
            } else {
                // Something is missing
                $className = is_object($object) ? get_class($object) : $object;
                throw new MissingParameter($className, $method, $reflectionParam->name);
            }
        }

        return $arguments;
    }

}