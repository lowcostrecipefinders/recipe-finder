<?php

namespace RecipeFinders\DependencyInjection\Exception;

class MissingParameter extends \Exception
{
    public function __construct($className, $methodName, $paramName)
    {
        parent::__construct(
          sprintf(
            "The container failed to call a method ('%s::%s'), because a required parameter ('%s') was missing",
            $className,
            $methodName,
            $paramName
          )
        );
    }
}