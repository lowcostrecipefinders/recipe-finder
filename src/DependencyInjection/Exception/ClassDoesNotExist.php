<?php

namespace RecipeFinders\DependencyInjection\Exception;

class ClassDoesNotExist extends \Exception
{
    public function __construct($className, \Exception $prev = null)
    {
        parent::__construct(
          sprintf(
            "The container failed to create an instance of '%s', because the class does not exist.",
            $className
          ), 0, $prev
        );
    }
}