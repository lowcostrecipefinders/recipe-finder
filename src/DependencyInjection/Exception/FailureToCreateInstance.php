<?php

namespace RecipeFinders\DependencyInjection\Exception;

class FailureToCreateInstance extends \Exception
{
    public function __construct($className, \Exception $prev = null)
    {
        parent::__construct(
          sprintf(
            "The container failed to create an instance of '%s'.",
            $className
          ), 0, $prev
        );
    }
}