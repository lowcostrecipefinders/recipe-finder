<?php

namespace RecipeFinders\DependencyInjection\Exception;

class MethodDoesNotExist extends \Exception
{
    public function __construct($className, $methodName)
    {
        parent::__construct(
          sprintf(
            "The container failed to call a method ('%s::%s'), because the method does not exist.",
            $className,
            $methodName
          )
        );
    }
}