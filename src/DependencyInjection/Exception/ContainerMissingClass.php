<?php

namespace RecipeFinders\DependencyInjection\Exception;

class ContainerMissingClass extends \Exception
{
    public function __construct($className, $methodName, $parameterName)
    {
        parent::__construct(
          sprintf(
            "The container failed to call a method ('%s::%s'), because it does not have a value for parameter '%s'.",
            $className,
            $methodName,
            $parameterName
          )
        );
    }
}