<?php

namespace RecipeFinders\Http;

/**
 * Class Session
 *
 * This class is responsible for handling the user's session
 *
 * @package RecipeFinders\Http
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Session
{
    /**
     * Constructor
     *
     */
    public function __construct()
    {
    }

    /**
     * Starts a new or resumes an old session
     *
     * @param bool $cycleFlash Whether or not to cycle the flash values
     */
    public function startSession($cycleFlash = false)
    {
        // Initialize the session
        session_start();
        if ($cycleFlash) {
            $this->cycleFlashValues();
        }
    }

    /**
     * Gets a value from the session.
     *
     * This method supports extraction of nested values using a period character as a separator between keys in each level.
     * For example, given the nested array ['upper' => ['mid' => ['lower' => 'SomeValue']]], calling query("upper.mid.lower")
     * will return the value 'SomeValue'.
     *
     * @param $key
     *
     * @return mixed|null null if the value doesn't exist in the session
     */
    public function get($key)
    {
        return $this->getFromArray($key, $_SESSION);
    }

    /**
     * Recursive function to extract nested values from an array
     *
     * @param string $key
     * @param array $array
     *
     * @return mixed|null
     */
    protected function getFromArray($key, array $array)
    {
        if (!$key || !is_string($key)) {
            return null;
        }

        $keyParts = explode(".", $key);
        $key = array_shift($keyParts);

        if (!array_key_exists($key, $array)) {
            return null;
        }

        $value = $array[$key];

        if (count($keyParts) === 0) {
            return $value;
        }

        if (!is_array($value)) {
            return null;
        }

        return $this->getFromArray(implode(".", $keyParts), $value);
    }

    /**
     * Sets a value in the session
     *
     * If inputting a nested value, this can be done by separating each layer of the key with a period character.
     * This works, so long as any of the keys within the nesting does not resolve to a non-array value.  What this
     * means is that this method will create arrays to fulfill the requiring nesting, but it will not overwrite an
     * existing value in order to properly create the nesting.
     *
     * @param string $key
     * @param mixed $value
     *
     * @return bool Whether or not the value was successfully set
     */
    public function set($key, $value)
    {
        if (!$key || !is_string($key)) {
            return false;
        }

        $keyParts = explode(".", $key);
        $array = &$_SESSION;

        // Handle nested sets
        while (count($keyParts) > 1) {
            $currentKey = array_shift($keyParts);
            if (array_key_exists($currentKey, $array))
            {
                if (!is_array($array[$currentKey])) {
                    // Don't overwrite an existing value!!
                    return false;
                }

                $array = &$array[$currentKey];
            } else {
                $array[$currentKey] = array();
                $array = &$array[$currentKey];
            }
        }

        $currentKey = array_shift($keyParts);
        $array[$currentKey] = $value;

        return true;
    }

    /**
     * Sets a flash value in the session that can be retrieved with the getFlash method
     *
     * @param string $key
     * @param mixed $value
     */
    public function setFlash($key, $value)
    {
        $this->set("__flash.new." . $key, $value);
    }

    /**
     * Gets a flash value from the session
     *
     * Looks first for a flash value that was set during the last request.
     * Falls back to a flash value set during the current request.
     * Returns null if the value does not exist.
     *
     * @param string $key
     *
     * @return mixed|null
     */
    public function getFlash($key)
    {
        $value = $this->get("__flash.old." . $key);
        return is_null($value) ? $this->get("__flash.new." . $key) : $value;
    }

    /**
     * Makes it so that a key that would be deleted during the next request won't be
     *
     * It will NOT overwrite a newly created flash value if it exists
     *
     * @param string $key
     *
     * @return bool Whether or not the value was saved
     */
    public function saveFlash($key)
    {
        // Don't overwrite a new flash value
        // If this needs to be done, it can be done manually
        if ($this->get('__flash.new.' . $key ) !== null) {
            return false;
        }
        $this->setFlash($key, $this->getFlash($key));
        return true;
    }

    /**
     * Updates flash values in the session.
     *
     * Old values (2 requests ago) are removed.
     * Values from the previous request become old values.
     *
     * This method should only be called once per request before any new flash values are created
     *
     * @return void
     */
    public function cycleFlashValues() {
        $newValues = $this->get("__flash.new");
        if (is_null($newValues)) {
            $newValues = array();
        }
        $this->set("__flash.old", $newValues);
        $this->set("__flash.new", array());
    }

    /**
     * Destroys the current session (i.e. When a user logs out)
     *
     * @return void
     */
    public function destroy()
    {
        session_destroy();
    }
}