<?php

namespace RecipeFinders\Http;
use RecipeFinders\Util\ParameterBag;

/**
 * Class Request
 *
 * The Request class is an Object-Oriented approach to handling http requests
 *
 * @package RecipeFinders\Http
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Request
{
    /** @type ParameterBag $request holds the values from the $_POST super global */
    protected $request;

    /** @type ParameterBag $query holds the values from the $_GET super global */
    protected $query;

    /** @type ParameterBag $server holds the values from the $_SERVER super global */
    protected $server;

    /** @type ParameterBag $files holds the values from the $_FILES super global */
    protected $files;

    /** @type ParameterBag $cookie holds the values from the $_COOKIE super global */
    protected $cookie;

    /** @type string[] */
    protected $pathParts;

    /** @type string */
    protected $rootPath;

    /**
     * Constructor
     *
     * @param bool   $initialize Whether or not to initialize the Request object from PHP Super Globals
     * @param string $rootPath   The root document path for the web site
     */
    public function __construct($initialize = true, $rootPath = '')
    {
        $this->rootPath = $rootPath;

        if ($initialize) {
            $this->initialize($_GET, $_POST, $_SERVER, $_FILES, $_COOKIE);
        }

    }

    /**
     * Initializes the Request object from parameter arrays
     *
     * @param array $get    Values from $_GET
     * @param array $post   Values from $_POST
     * @param array $server Values from $_SERVER
     * @param array $files  Values from $_FILES
     * @param array $cookie Values from $_COOKIE
     */
    public function initialize(array $get, array $post, array $server, array $files, array $cookie)
    {
        $this->query = new ParameterBag($get);
        $this->request = new ParameterBag($post);
        $this->server = new ParameterBag($server);
        $this->files = new ParameterBag($files);
        $this->cookie = new ParameterBag($cookie);

        // Determine the path
        $uri = $this->server->get("REQUEST_URI");
        $path = parse_url($uri)['path'];
        if ($this->rootPath != '') {
            // Make the path relative to the site within the directory structure
            $path = str_replace($this->rootPath, '', $path);
            echo "PATH " . $path . "<br>";
        }
        $path = trim($path, "/");

        $this->pathParts = explode("/", $path);
    }

    /**
     * Returns the request method for the current http request
     *
     * @return string (lower case)
     */
    public function method()
    {
        $method = strtolower($this->server->get('REQUEST_METHOD'));
        if ('post' === $method && $this->request()->has('_method')) {
            $method = strtolower($this->request()->get('_method'));
        }
        return $method;
    }

    /**
     * Returns the parameter bag for the request
     *
     * @return ParameterBag
     */
    public function request()
    {
        return $this->request;
    }

    /**
     * Returns the parameter bag for the query
     *
     * @return ParameterBag
     */
    public function query()
    {
        return $this->query;
    }

    /**
     * Returns the parameter bag for the server variables
     *
     * @return ParameterBag
     */
    public function server()
    {
        return $this->server;
    }

    /**
     * Returns the parameter bag for the files super global
     *
     * @return ParameterBag
     */
    public function files()
    {
        return $this->files;
    }

    /**
     * Returns the parameter bag for the files in the cookie super global
     *
     * @return ParameterBag
     */
    public function cookie()
    {
        return $this->cookie;
    }

    /**
     * Determines if the current request is a GET request
     *
     * @return bool
     */
    public function isGet()
    {
        return $this->isMethod('get');
    }

    /**
     * Determines if the current request is a POST request
     *
     * @return bool
     */
    public function isPost()
    {
        return $this->isMethod('post');
    }

    /**
     * Determines if the current request is a PUT request
     *
     * @return bool
     */
    public function isPut()
    {
        return $this->isMethod('put');
    }

    /**
     * Determines if the current request is a DELETE request
     *
     * @return bool
     */
    public function isDelete()
    {
        return $this->isMethod('delete');
    }

    /**
     * Checks to see if the current request is the method specified
     *
     * @param string $method
     *
     * @return bool
     */
    public function isMethod($method)
    {
        return $this->method() === strtolower($method);
    }

    /**
     * Gets the parts of the path for the request relative to the document root
     *
     * @return string[]
     */
    public function getPathParts()
    {
        return $this->pathParts;
    }

    /**
     * @return string
     */
    function getBaseUrl() {
        $rootPath = $this->rootPath;
        $protocol = $this->server->has('HTTPS') && $this->server->get('HTTPS') != 'off' ? 'https://' : 'http://';
        $host = $this->server->get('HTTP_HOST');
        return $protocol . $host . $rootPath;
    }
}