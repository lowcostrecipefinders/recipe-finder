<?php

namespace RecipeFinders\Http;

/**
 * Class Response
 *
 * @package RecipeFinders\Http
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Response
{
    // Good
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NO_CONTENT = 204;

    // Redirect
    const HTTP_MOVED_PERMANENTLY = 301;
    const HTTP_SEE_OTHER = 303;             // This is for redirects with query
    const HTTP_TEMPORARY_REDIRECT = 307;    // Redirects but uses same request method
    // Not found
    const HTTP_UNAUTHORIZED = 401;          // When authentication has not been established
    const HTTP_FORBIDDEN = 403;             // When not allowed and authentication is pointless
    const HTTP_NOT_FOUND = 404;

    // Errors
    const HTTP_INTERNAL_SERVER_ERROR = 500;


    protected $headers = array();
    protected $body = '';
    protected $responseCode;

    public function __construct($body, $code = self::HTTP_OK, $headers = array())
    {
        $this->body = $body;
        $this->responseCode = $code;
        $this->headers = $headers;
    }

    public function setHeader($key, $value)
    {
        $this->headers[$key] = $value;
    }

    public function setResponseCode($code)
    {
        $this->responseCode = $code;
    }

    public function flush()
    {
        http_response_code($this->responseCode);

        foreach($this->headers as $header => $value) {
            header($header . ": " . $value);
        }

        if (floor($this->responseCode / 100) === 3) {
            return;
        }

        echo $this->body;
    }
}