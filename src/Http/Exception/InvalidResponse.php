<?php

namespace RecipeFinders\Http\Exception;

class InvalidResponse extends \Exception
{
    public function __construct()
    {
        parent::__construct("A controller action should return a Response object, a string, or false");
    }
}