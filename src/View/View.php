<?php

namespace RecipeFinders\View;

use RecipeFinders\DependencyInjection\Container;
use RecipeFinders\DependencyInjection\ContainerAware;

class View implements ContainerAware
{

    /** @type array $helpers All the helpers for the view */
    protected $helpers = array();

    /** @type Container $container */
    protected $container;

    /**
     * Constructor
     */
    public function __construct()
    {
        // Empty
        // We could load helpers in the constructor if we wanted to
        // But it's probably a better idea to do it externally
    }

    /**
     * @param string $_file             The name of the file to be rendered
     * @param array $_data              An array of data that can be used by the rendered template
     * @param object $_controller       The controller that called this method
     *
     * @return string                   The rendered view
     */
    public function renderFile($_file, array $_data = array(), $_controller = null)
    {

        // Get rid of anything that has previously been echoed
        // It can still be referenced within the view
        $__dumped = ob_get_clean(); // do i really want to do this??
        ob_start();

        // Bring the helpers into the scope
        extract($this->helpers);

        $_session = $this->container->get('session');
        $_env = $_environment = $this->container->get('environment');

        // Bring the data into scope
        extract($_data);

        include($_file);

        $result = ob_get_clean();
        ob_start();
        return $result;
    }

    /**
     * @param string $name   A name for the helper that can be used in the view
     * @param mixed  $helper Anything that you want access to in the view
     *                       Normally this will be either an object or a function
     *                       that can be used in the view for some utility task
     *
     * @return void
     */
    public function addHelper($name, $helper)
    {

        // The name can't be empty
        if (strlen($name) < 1)  {
            return;
        }

        // The name is always saved with an underscore prefixed
        // This makes loading the helpers into the view easier
        // With less chance of muddling up the scope's symbol table
        $name = $name[0] === '_' ? $name : '_' . $name;

        $this->helpers[$name] = $helper;
    }

    /**
     * Returns a helper with the given name
     *
     * @param string $name The name of the helper
     *
     * @return mixed|null The helper with the given name
     */
    public function getHelper($name)
    {
        // The name can't be empty
        if (strlen($name) < 1)  {
            return null;
        }

        // The helpers are saved with an underscore prefixed
        // But you don't need to use the underscore prefix
        // In order to retrieve it from the the View object
        $name = $name[0] === '_' ? $name : '_' . $name;

        return isset($this->helpers[$name]) ? $this->helpers[$name] : null;
    }

    /**
     * @param string $fileName The name of the file to include
     *
     * @return void
     */
    public function loadHelpersFromFile($fileName)
    {
        $view = $this;
        $container = $this->container;
        if (file_exists($fileName)) {
            include($fileName);
        }
    }

    /**
     * Set the container in the object
     *
     * @param Container $container The container object that will be injected
     *
     * @return void
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }
}