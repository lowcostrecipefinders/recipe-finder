<?php
namespace RecipeFinders\IngredientPricer;
use PHPExcel_Cell_DataType;
use PHPExcel_Reader_Excel2007;
use RecipeFinders\Model\IngredientPricerInterface;
use RecipeFinders\Model\LocationInterface;

/**
 * Class IngredientPricer
 * @package RecipeFinders\IngredientPricer
 * @author: Ryan Ford <fordr@onid.oregonstate.edu>
 */
class IngredientPricer implements IngredientPricerInterface
{

    protected $mysqli;
	protected $objPHPExcel;

    public function  __construct($mysqli)
    {
		$objReader = new PHPExcel_Reader_Excel2007();
        $this->mysqli = $mysqli;
        $this->objPHPExcel = $objReader->load(__DIR__ . '/assets/Ingredients.xlsx');
    }

     /**
     * Takes a list of ingredient names and returns pricing data for them
     *
     * @param  string[]          $ingredientList  A list of ingredient names
     *
     * @param  LocationInterface $location        
     *
     * @return float             $results[] An associative array where the key
     *                           is the ingredient name and the value is a price.
     */
    public function getPriceForIngredients(array $ingredientList, LocationInterface $location = null)
    {
        $results = array();    
        foreach($ingredientList as $val) {
            if(($price = $this->getDBPrice($val)) != -1) {
                $results[$val] = $price;
            }
            else {
                $price = $this->lookupPrice($val);
                $this->insertDB($val, $price);
                $results[$val] = $price;
            }
        }
		return $results;
    }

    /**
     * Searches the ingredient database for an ingredient's price.
     *
     * @param string $ingredient  The ingredient to search for
     *
     * @return float $price  The price of the ingredient if found, -1 if not found.
     */
    protected function getDBPrice($ingredient)
    {
        if($this->mysqli->connect_errno) {
            return -1;
        }

        $query = "SELECT * FROM ingredient WHERE name = '$ingredient'";
        $res = mysqli_query($this->mysqli, $query);

        if(mysqli_num_rows($res)) {
            $results = mysqli_fetch_array($res);
            return $results[1];
        }
        else {
            return -1;
        }
    }

    /**
     * Attempts to find the closest matching price for a given ingredient.
     *
     * @param string $ingredient  The name of the ingredient to find a price for.
     *
     * @return string $price  If found, the closest matching price for the ingredient.
     *                        Else, returns 0.
     */
    protected function lookupPrice($ingredient)
    {
        $price = 0.00;
        $row_index = 2;
        $result_count = 0;
        $highest_count = 0;
        $lowest_count = 100;
        $best_result = -1;
        $name_column = 'A';
        $price_column = 'B';
        $count_column = 'C';
        $calc_column = 'D';

        // Split the ingredient into an array of lowercase words
        $ingredient = strtolower($ingredient);
        $search_terms = preg_split("/[\s,-]/", $ingredient);

        // Filter 1: Count the number of words in each ingredient name description
        while(($this->objPHPExcel->getActiveSheet()->getCell($name_column . $row_index)->getValue()) != '') {
            $this->objPHPExcel->getActiveSheet()->setCellValue($count_column . $row_index,
                '=LEN(' . $name_column . $row_index . ')-LEN(SUBSTITUTE(' . $name_column . $row_index . '," ",""))+1', 
                PHPExcel_Cell_DataType::TYPE_STRING);
            $row_index++;
        }
        $row_index = 2;

        // Filter 2 ... n: Search for search terms in each ingredient name description
        foreach($search_terms as $val) {
            while(($this->objPHPExcel->getActiveSheet()->getCell($name_column . $row_index)->getValue()) != '') {
                $this->objPHPExcel->getActiveSheet()->setCellValue($calc_column . $row_index,
                    '=IF(ISNUMBER(FIND("' . $val . '",' . $name_column . $row_index . ')), "TRUE")', 
                    PHPExcel_Cell_DataType::TYPE_STRING);
                $row_index++;
            }
            $row_index = 2;
            $prev_column = $calc_column;
            $calc_column++;
        }

        // Find the best result; the result with the highest number of search terms using the least amount of words
        while(($this->objPHPExcel->getActiveSheet()->getCell($name_column . $row_index)->getValue()) != '') {
            // Count the number of search terms that were in the ingredient name
            $this->objPHPExcel->getActiveSheet()->setCellValue($calc_column . $row_index,
                '=COUNTIF(D' . $row_index . ':' . $prev_column . $row_index .', "TRUE")', 
                PHPExcel_Cell_DataType::TYPE_STRING);
            // If there were more matches than the current best match, set as the new best result
            if(($this->objPHPExcel->getActiveSheet()->getCell($calc_column . $row_index)->getCalculatedValue()) > $highest_count) {
                $best_result = $row_index;
                $lowest_count = $this->objPHPExcel->getActiveSheet()->getCell($count_column . $row_index)->getCalculatedValue();
                $highest_count = $this->objPHPExcel->getActiveSheet()->getCell($calc_column . $row_index)->getCalculatedValue();
            }
            // Else if it matches the best result, but is composed of less words (simpler ingredient), 
            // make the simpler ingredient the best result
            else if(($this->objPHPExcel->getActiveSheet()->getCell($calc_column . $row_index)->getCalculatedValue()) == $highest_count) {
                if(($this->objPHPExcel->getActiveSheet()->getCell($count_column . $row_index)->getCalculatedValue()) < $lowest_count) {
                    $best_result = $row_index;
                    $lowest_count = $this->objPHPExcel->getActiveSheet()->getCell($count_column . $row_index)->getCalculatedValue();
                }
            }
            $row_index++;
        }

        // If a match was found, return the best match's price
        if($best_result != -1) {
            $price = $this->objPHPExcel->getActiveSheet()->getCell($price_column . $best_result)->getCalculatedValue();
        }

        return $price;
    }

    /**
     * Inserts an ingredient and its price into the ingredients database.
     *
     * @param string $ingredient  The name of the ingredient to insert
     * @param float $price  The price of the ingredient to insert
     *
     * @return none
     */
    protected function insertDB($ingredient, $price)
    {
        $query = "INSERT INTO ingredient(name, price) VALUES('$ingredient', '$price')";
        $res = mysqli_query($this->mysqli, $query);
    }
}
