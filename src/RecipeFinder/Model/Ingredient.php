<?php

namespace RecipeFinders\RecipeFinder\Model;

use RecipeFinders\Model\IngredientInterface;

/**
 * Class Recipe
 * @package RecipeFinders\RecipeRecipe\Model
 * @author  Nathan Mize <mizen@onid.oregonstate.edu>
 * @author  Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Ingredient implements IngredientInterface
{
    /** @var string $name */
    protected $name;

    /** @type string quantity*/
    protected $quantity;

    /** @type string Units of measurements */
    protected $units;



    /**
     * Constructor
     *
     * @param string             $name
     * @param string             $quantity
     * @param string             $units
     */
    public function __construct($name = '', $quantity = '', $units = '')
    {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->units = $units;
    }

    public function getName(){

        return $this->name;
    }

    public function getQuantity(){
    
        return $this->quantity;
    }

    public function getUnitOfMeasurement(){
        
        return $this->units;
    }
}
