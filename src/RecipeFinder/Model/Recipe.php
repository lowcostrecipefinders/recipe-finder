<?php

namespace RecipeFinders\RecipeFinder\Model;

use RecipeFinders\Model\IngredientInterface;
use RecipeFinders\Model\RecipeInterface;
use RecipeFinders\Measurements\Units;
use RecipeFinders\RecipeRanker\Model\PricedIngredient;

/**
 * Class Recipe
 * @package RecipeFinders\RecipeRecipe\Model
 * @author  Nathan Mize <mizen@onid.oregonstate.edu>
 * @author  Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Recipe implements RecipeInterface
{
    /** @var string $name */
    protected $name;

    /** @type PricedIngredient[] $ingredients */
    protected $ingredients = array();

    /** @type string  $id */
    protected $id;

    /** @type int $ranking */
    protected $rank;

    /**
     * Constructor
     *
     * @param string             $name
     * @param string             $id
     * @param PricedIngredient[] $ingredients
     */
    public function __construct($name = '', $id = '', array $ingredients = array(), $servings = '')
    {
        $this->name = $name;
        $this->id = $id;
        $this->servings = $servings;

        $ingredients_arr = array();

        foreach ($ingredients as $ingredient){

            // two types of ingredients
            // "2 celery ribs, diced",
            // "3/4 cup mayonnaise",
            // check for ',' in the ingredient name
            $comma = '/\,/';
            $ingredient_pattern = '';

            $name = '';
            $units = '';
            $quantity = '';
            $matches = array();

            if(preg_match($comma, $ingredient)){
                $ingredient_pattern = '/^(.*?)\s(.*?)\,\s(.*?)$/';
                preg_match($ingredient_pattern, $ingredient, $matches);

                if( count($matches) == 4){
                    $name = $matches[2];
                    $quantity = $matches[1];
                    $units = Units::detectUnitFromString($ingredient);
                }

            }
            else {
                $ingredient_pattern = '/^(.*?)\s(.*?)\s(.*?)$/';
                preg_match($ingredient_pattern, $ingredient, $matches);

                if( count($matches) == 4){
                    $name = $matches[3];
                    $quantity = $matches[1];
                    $units = Units::detectUnitFromString($ingredient);
                }

            }

            // convert frac into float
            $frac = array();
            if(preg_match('/^(.*?)\/(.*?)$/', $quantity, $frac)){
                $quantity = $frac[1] / $frac[2];
            }

            $new_ingredient = new Ingredient($name, $quantity, $units);
            array_push($ingredients_arr, $new_ingredient);

           // print $new_ingredient->getName()."\t";
           // print $new_ingredient->getQuantity()."\t";
           // print $new_ingredient->getUnitOfMeasurement()."\n";

        }

        $this->ingredients = $ingredients_arr;

    }

    public function getName(){

        return $this->name;
    }

    public function getIngredients(){
    
        return $this->ingredients;
    }

    public function getServings(){

        return $this->servings;
    }

    public function getId(){

        return $this->id;
    }
}
