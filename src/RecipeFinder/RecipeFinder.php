<?php

namespace RecipeFinders\RecipeFinder;

use RecipeFinders\Model\RecipeInterface;
use RecipeFinders\Model\RecipeFinderInterface;
use RecipeFinders\RecipeFinder\Model\Recipe;

class RecipeFinder implements RecipeFinderInterface
{

    public function findRecipeById($id)
    {
        // General recipe request
        // https://developer.yummly.com/documentation/search-recipes-response-sample
        //
        // Request by ID
        // https://developer.yummly.com/documentation/get-recipe-response-sample
        $baseURL = "http://api.yummly.com/v1/api/recipe/";
        $api_key = "?_app_id=dd2de133&_app_key=7d2b495cde101af4d0263d7f4ec92dfc&";
        $json = file_get_contents($baseURL.$id.$api_key);

        return json_decode($json);

    }

    public function findRecipes($query)
    {
        // assume query is just a string for now. 
        // we could send some structured data later to 
        // form a smarter api rquest
        $baseURL = "http://api.yummly.com/v1/api/recipes?_app_id=dd2de133&_app_key=7d2b495cde101af4d0263d7f4ec92dfc&q=";

        $json = file_get_contents($baseURL.$query);

        $recipes = json_decode($json);

        //print var_dump($recipes);

        $recipes_arr = array();
        foreach ($recipes->{'matches'} as $recipe){

            $id_data = $this->findRecipeById($recipe->{'id'});
            
            $new_recipe = new Recipe(
                $recipe->{'recipeName'},
                $recipe->{'id'},
                $id_data->{'ingredientLines'},
                $id_data->{'numberOfServings'}
            );

            array_push($recipes_arr, $new_recipe);
        }

        return $recipes_arr;
    }

}
