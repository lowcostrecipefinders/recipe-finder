<?php

namespace RecipeFinders\RecipeRanker;

use RecipeFinders\Measurements\Converter;
use RecipeFinders\Measurements\Units;
use RecipeFinders\Model\IngredientPriceInterface;
use RecipeFinders\Model\IngredientPricerInterface;
use RecipeFinders\Model\LocationInterface;
use RecipeFinders\Model\RecipeInterface;
use RecipeFinders\Model\RecipeRankerInterface;
use RecipeFinders\RecipeRanker\Model\PricedIngredient;
use RecipeFinders\RecipeRanker\Model\RankedRecipe;

/**
 * Class RecipeRanker
 * @package RecipeFinders\RecipeRanker
 * @author Nathan Mize <mizen@onid.oregonstate.edu>
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class RecipeRanker implements RecipeRankerInterface
{

    /** @type \RecipeFinders\Model\IngredientPricerInterface  */
    protected $pricer;

    public function __construct(IngredientPricerInterface $pricer, Converter $converter)
    {
        $this->pricer = $pricer;
        $this->converter = $converter;
    }

    /**
     * Returns an array of ranked recipes sorted in rank order
     *
     * @param RecipeInterface[]         $recipes  The recipes to rank
     * @param LocationInterface|null    $location The location where the user is located (for pricing data)
     *                                            TODO: Should this be required??
     * @param int|null                  $quantity The number of recipes to return
     *                                            Used for pagination.
     *                                            $quantity <= 0 will return all recipes
     * @param int                       $offset   An offset for the recipes returned
     *                                            Used for pagination
     *                                            Only used if $quantity is > 0
     * @return RankedRecipe[]
     */
    public function rankRecipes(array $recipes, LocationInterface $location = null, $quantity = 0, $offset = 0)
    {
        $ingredients = $this->extractIngredientListFromRecipes($recipes);
        $prices = $this->pricer->getPriceForIngredients($ingredients, $location);
        $pricedRecipes = $this->createRankedRecipes($recipes, $prices);
        $this->sortPricedRecipes($pricedRecipes);
        $this->setRecipeRanks($pricedRecipes);

        if ($quantity) {
            $offset = $offset > 0 ? $offset : 0;
            $rankedRecipes = array_slice($pricedRecipes, $offset, $quantity);
            return $this->setRecipeRanks($rankedRecipes, $offset + 1);
        }

        return $this->setRecipeRanks($pricedRecipes);
    }

    /**
     * Extracts a list of ingredients from the given recipes
     *
     * Basically, it iterates over all the ingredients of each recipe,
     * and if it finds an ingredient name that it hasn't already found,
     * it adds it to the list.
     *
     * @param RecipeInterface[] $recipes  The recipes
     *
     * @return string[]  A list of all the unique ingredients in the recipes
     */
    protected function extractIngredientListFromRecipes(array $recipes)
    {
        /** @var array $ingredientList  A set of ingredients where the key is the ingredient name */
        $ingredientList = array();

        // Search through all the recipes and their ingredients.
        foreach($recipes as $recipe) {
            foreach($recipe->getIngredients() as $ingredient) {
                // Use the $ingredientList as a set
                // This is probably more efficient than having the ingredient list
                // As a numeric array and searching for uniqueness.
                // Making the key lower case might avoid duplicating lookups later
                // For the ingredient pricer
                $name = strtolower($ingredient->getName());
                $ingredientList[strtolower($name)] = true;
            }
        }

        // Return the keys (the ingredient names)
        return array_keys($ingredientList);
    }

    /**
     * Takes recipes and converts them into ranked recipes with the given prices data
     *
     * @param RecipeInterface[]          $recipes  The recipes
     * @param IngredientPriceInterface[] $prices   An associative array of ingredient prices where
     *                                             the key is the ingredient name
     *
     * @return RankedRecipe[]
     */
    protected function createRankedRecipes(array $recipes, array $prices)
    {
        $rankedRecipes = array();

        foreach($recipes as $recipe){
            $pricedIngredients = array();
            foreach($recipe->getIngredients() as $ingredient) {
                $key = strtolower($ingredient->getName());
                $price = $prices[$key];
                $convertedQuantity = $this->converter->convert(Units::GRAM, 1, $ingredient->getUnitOfMeasurement());
                $adjustedPrice = $convertedQuantity > 0 ? ($price / $convertedQuantity) : 0;
                $pricedIngredients[] = new PricedIngredient($ingredient, $adjustedPrice);
            }

            $rankedRecipes[] = RankedRecipe::newFromRecipe($recipe, $pricedIngredients);
        }
        return $rankedRecipes;
    }

    /**
     * Sorts the recipes by rank
     *
     * @param RankedRecipe[] $pricedRecipes  The array of recipes to be sorted
     *
     * @return void
     */
    protected function sortPricedRecipes(array &$pricedRecipes)
    {
        $sortFunction = function(RankedRecipe $a, RankedRecipe $b) {
            $costA = $a->getCostPerPersonFed();
            $costB = $b->getCostPerPersonFed();

            if ($costA === $costB) {
                return 0;
            } else if ($costA < $costB) {
                return -1;
            }
            return 1;
        };

        usort($pricedRecipes, $sortFunction);
    }

    /**
     * Sets the ranks for the recipes (1,2,3...)
     *
     * Can use a different starting number by setting $position
     *
     * @param  RankedRecipe[] $pricedRecipes   A sorted list of recipes
     * @param  int            $position        Where to start counting the ranks from
     * @return RankedRecipe[]
     */
    protected function setRecipeRanks(array $pricedRecipes, $position = 1)
    {
        foreach($pricedRecipes as $recipe) {
            $recipe->setRank($position);
            $position++;
        }

        return $pricedRecipes;
    }
}