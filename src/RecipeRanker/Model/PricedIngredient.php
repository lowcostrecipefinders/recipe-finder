<?php

namespace RecipeFinders\RecipeRanker\Model;

use RecipeFinders\Model\IngredientInterface;
use RecipeFinders\Model\PricedIngredientInterface;

/**
 * Class PricedIngredient
 * @package RecipeFinders\RecipeRanker\Model
 * @author  Nathan Mize <mizen@onid.oregonstate.edu>
 * @author  Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class PricedIngredient implements PricedIngredientInterface
{
    /** @type IngredientInterface  */
    protected $ingredient;

    /** @type float $costPerUnit */
    protected $costPerUnit;


    /**
     * Constructor
     *
     * @param IngredientInterface $ingredient
     * @param float               $costPerUnit
     */
    public function __construct(IngredientInterface $ingredient = null, $costPerUnit = 0.0)
    {
        $this->ingredient = $ingredient;
        $this->costPerUnit = $costPerUnit;
    }

    /**
     * Returns the name of the ingredient
     *
     * @return string
     */
    public function getName()
    {
        return $this->ingredient->getName();
    }

    /**
     * Returns the quantity of the ingredient used in the recipe in the proper unit of measurement
     *
     * @return int|float
     */
    public function getQuantity()
    {
        return $this->ingredient->getQuantity();
    }

    /**
     * Returns the unit of measurement
     *
     * @return string    The units of measurement should be defined as constants somewhere
     */
    public function getUnitOfMeasurement()
    {
        return $this->ingredient->getUnitOfMeasurement();
    }

    /**
     * Returns the price per unit times the quantity
     *
     * @return float
     */
    public function getTotalCost()
    {
        return $this->getCostPerUnit() * $this->getQuantity();
    }

    /**
     * Returns the cost per unit of measurement
     *
     * @return float
     */
    public function getCostPerUnit()
    {
        return $this->costPerUnit;
    }

    /**
     * @param float $pricePerUnit
     */
    public function setCostPerUnit($pricePerUnit)
    {
        $this->costPerUnit = $pricePerUnit;
    }

    /**
     * @param \RecipeFinders\Model\IngredientInterface $ingredient
     */
    public function setBaseIngredient(IngredientInterface $ingredient)
    {
        $this->ingredient = $ingredient;
    }
}