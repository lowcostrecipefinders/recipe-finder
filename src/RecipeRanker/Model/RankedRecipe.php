<?php

namespace RecipeFinders\RecipeRanker\Model;

use RecipeFinders\Model\IngredientInterface;
use RecipeFinders\Model\PricedIngredientInterface;
use RecipeFinders\Model\RankedRecipeInterface;
use RecipeFinders\Model\RecipeInterface;

/**
 * Class RankedRecipe
 * @package RecipeFinders\RecipeRanker\Model
 * @author  Nathan Mize <mizen@onid.oregonstate.edu>
 * @author  Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class RankedRecipe implements RankedRecipeInterface
{
    /** @var string $name */
    protected $name;

    /** @type int $numberOfPeopleFed */
    protected $numberOfPeopleFed;

    /** @type PricedIngredient[] $ingredients */
    protected $ingredients = array();

    /** @type string  $id */
    protected $id;

    /** @type int $ranking */
    protected $rank;

    /**
     * Constructor
     *
     * @param string             $name
     * @param int                $numberOfPeopleFed
     * @param string             $id
     * @param PricedIngredient[] $ingredients
     */
    public function __construct($name = '', $numberOfPeopleFed = 0, $id = '', array $ingredients = array())
    {
        $this->name = $name;
        $this->numberOfPeopleFed = $numberOfPeopleFed;
        $this->ingredients = $ingredients;
        $this->id = $id;
    }

    /**
     * Acts as an alternate constructor for creating this class from an existing recipe object
     *
     * @param  RecipeInterface $recipe
     * @param  array           $ingredients
     *
     * @return static
     */
    public static function newFromRecipe(RecipeInterface $recipe, array $ingredients = array())
    {
        return new static($recipe->getName(), $recipe->getServings(), $recipe->getId(), $ingredients);
    }

    /**
     * Returns an array of ingredients that are priced
     *
     * @return PricedIngredientInterface[]
     */
    public function getPricedIngredients()
    {
        return $this->ingredients;
    }

    /**
     * Returns the sum of the total costs of its ingredients
     *
     * @return float
     */
    public function getTotalCost()
    {
        $total = 0;
        foreach($this->getPricedIngredients() as $ingredient) {
            $total += $ingredient->getTotalCost();
        }

        return $total;
    }

    /**
     * Returns the total cost divided by the number of people the recipe feeds
     *
     * @return float
     */
    public function getCostPerPersonFed()
    {
        if ($this->getNumberOfPeopleFed() <= 0) {
            // TODO: What should happen????
            return $this->getTotalCost();
        }

        return $this->getTotalCost() / $this->getNumberOfPeopleFed();
    }

    /**
     * Returns the ranking for the recipe
     *
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }

    /**
     * Returns the name of the recipe
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns how many people are supposed to be fed by this recipe
     *
     * I believe this is important for ranking.  A recipe that costs
     * $2.00, but only feeds one person should be ranked lower than
     * a recipe that costs $6.00 but feeds 8.
     *
     * If this data isn't available, we will need to improvise.
     *
     * @return int
     */
    public function getNumberOfPeopleFed()
    {
        return $this->numberOfPeopleFed;
    }

    /**
     * Returns an array of the ingredients needed for the recipe
     *
     * @return IngredientInterface[]
     */
    public function getIngredients()
    {
        return $this->getPricedIngredients();
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param int $numberOfPeopleFed
     */
    public function setNumberOfPeopleFed($numberOfPeopleFed)
    {
        $this->numberOfPeopleFed = $numberOfPeopleFed;
    }

    /**
     * @param \RecipeFinders\RecipeRanker\Model\PricedIngredient[] $ingredients
     */
    public function setIngredients(array $ingredients)
    {
        $this->ingredients = $ingredients;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param mixed $rank
     */
    public function setRank($rank)
    {
        $this->rank = $rank;
    }

    /**
     * Returns the number of servings for this recipe
     *
     * I believe this is important for ranking.  A recipe that costs
     * $2.00, but only feeds one person should be ranked lower than
     * a recipe that costs $6.00 but feeds 8.
     *
     * If this data isn't available, we will need to improvise.
     *
     * @return int
     */
    public function getServings()
    {
        return $this->getNumberOfPeopleFed();
    }
}