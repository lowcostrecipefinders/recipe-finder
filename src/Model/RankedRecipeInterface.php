<?php

namespace RecipeFinders\Model;

interface RankedRecipeInterface extends PricedRecipeInterface
{
    /**
     * Returns the ranking for the recipe
     *
     * @return float
     */
    public function getRank();
}