<?php

namespace RecipeFinders\Model;

/**
 * Interface PricedRecipeInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface PricedRecipeInterface extends RecipeInterface
{
    /**
     * Returns an array of ingredients that are priced
     *
     * @return PricedIngredientInterface[]
     */
    public function getPricedIngredients();

    /**
     * Returns the sum of the total costs of its ingredients
     *
     * @return float
     */
    public function getTotalCost();

    /**
     * Returns the total cost divided by the number of people the recipe feeds
     *
     * @return float
     */
    public function getCostPerPersonFed();


}