<?php

namespace RecipeFinders\Model;

/**
 * Interface RecipeFinderInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface RecipeFinderInterface
{
    /**
     * Gets a recipe by a unique identifier
     *
     * @param  string $id a unique identifier for the recipe
     *
     * @return RecipeInterface|null   Null if the recipe couldn't be found
     */
    public function findRecipeById($id);

    /**
     * Finds recipes from a user query
     *
     * @param  string $query  The search query
     *                        TODO: This interface might be changed to support a canonical form
     *                        of query rather than a raw query string.  The implementers should
     *                        decide on the format they want the query to be in.
     *
     * @return RecipeInterface[]
     * @throws RecipeLookupError   Throws an exception when there is an error searching for recipes
     *                             TODO: Implementers should Create the RecipeLookupError Exception
     */
    public function findRecipes($query);

}
