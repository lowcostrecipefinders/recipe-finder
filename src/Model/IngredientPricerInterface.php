<?php

namespace RecipeFinders\Model;

/**
 * Interface IngredientPricerInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface IngredientPricerInterface
{
    /**
     * Takes a list of ingredient names and returns pricing data for them
     *
     * @param  string[]          $ingredientList  A list of ingredient names
     *                                            TODO: Implementers, is this what you need, or will
     *                                                  you need something else?
     * @param  LocationInterface $location        The user's location to get location specific pricing data
     *                                            TODO: Should this be required??  Implementers decide
     *
     * @return float[]                            An associative array where the key is the ingredient name
     *                                            and the value is the price per gram
     */
    public function getPriceForIngredients(array $ingredientList, LocationInterface $location = null);
}