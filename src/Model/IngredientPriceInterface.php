<?php

namespace RecipeFinders\Model;

/**
 * Interface IngredientPriceInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface IngredientPriceInterface
{
    /**
     * Returns the price for the ingredient per unit of measurement
     *
     * @return float
     */
    public function getPrice();

    /**
     * Returns the unit of measurement for the ingredient
     *
     * @return string  The unit of measurement.
     *                 These should be defined as constants somewhere to support conversion algorithms
     */
    public function getUnitOfMeasurement();
}