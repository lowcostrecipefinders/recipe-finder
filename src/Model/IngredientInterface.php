<?php

namespace RecipeFinders\Model;

/**
 * Interface IngredientInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface IngredientInterface
{
    /**
     * Returns the name of the ingredient
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the quantity of the ingredient used in the recipe in the proper unit of measurement
     *
     * @return int|float
     */
    public function getQuantity();

    /**
     * Returns the unit of measurement
     *
     * @return string    The units of measurement should be defined as constants somewhere
     */
    public function getUnitOfMeasurement();

}