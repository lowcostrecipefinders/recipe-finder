<?php

namespace RecipeFinders\Model;

/**
 * Interface PricedIngredientInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface PricedIngredientInterface extends IngredientInterface
{
    /**
     * Returns the cost per unit of measurement
     *
     * @return float
     */
    public function getCostPerUnit();

    /**
     * Returns the price per unit times the quantity
     *
     * @return float
     */
    public function getTotalCost();

}