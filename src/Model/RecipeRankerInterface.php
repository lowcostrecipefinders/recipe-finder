<?php

namespace RecipeFinders\Model;

interface RecipeRankerInterface
{

    /**
     * Returns an array of ranked recipes sorted in rank order
     *
     * @param RecipeInterface[]         $recipes  The recipes to rank
     * @param LocationInterface|null    $location The location where the user is located (for pricing data)
     *                                            TODO: Should this be required??
     * @param int|null                  $quantity The number of recipes to return
     *                                            Used for pagination.
     *                                            $quantity <= 0 will return all recipes
     * @param int                       $offset   An offset for the recipes returned
     *                                            Used for pagination
     *                                            Only used if $quantity is > 0
     * @return RankedRecipeInterface[]
     */
    public function rankRecipes(array $recipes, LocationInterface $location = null, $quantity = 0, $offset = 0);
}