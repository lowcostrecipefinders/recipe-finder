<?php

namespace RecipeFinders\Model;

/**
 * Interface RecipeInterface
 * @package RecipeFinders\Model
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
interface RecipeInterface
{
    /**
     * Returns an array of the ingredients needed for the recipe
     *
     * @return IngredientInterface[]
     */
    public function getIngredients();

    /**
     * Returns the name of the recipe
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the number of servings for this recipe
     *
     * I believe this is important for ranking.  A recipe that costs
     * $2.00, but only feeds one person should be ranked lower than
     * a recipe that costs $6.00 but feeds 8.
     *
     * If this data isn't available, we will need to improvise.
     *
     * @return int
     */
    public function getServings();

    /**
     * Returns a unique identifier for the recipe that can be used to find it again
     *
     * @return string
     */
    public function getId();


    // TODO:  If there are any additional fields that can be reliably gotten for recipes, such as
    //        a list of preparation steps, a description, or an image url, we should definitely add
    //        them to this interface. Just make sure everyone knows about the changes.
}
