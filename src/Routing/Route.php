<?php

namespace RecipeFinders\Routing;

use RecipeFinders\Http\Request;

/**
 * Class Route
 *
 * @package RecipeFinders\Routing
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Route
{

    /** @type string $name */
    protected $name;

    /** @type (string|RouteParameter)[] $parts  */
    protected $parts;

    /** @type string[] $types The request types that match this route */
    protected $types;

    /** @type string $controllerAction The name of the method for the controller */
    protected $controllerAction;

    /** @type string $controllerClass The name of the controller class */
    protected $controllerClass;

    /**
     * Constructor
     *
     * @param string[] $types      The request types this route matches
     * @param string   $pattern    The pattern this route matches
     * @param string   $name       The name of this route
     * @param string   $action     The name of the controller's method to call when this route matches
     * @param string   $controller The class name of the controller for this route
     */
    public function __construct(array $types, $pattern, $name, $action, $controller)
    {
        $this->types = $types;
        $this->parts = $this->parsePattern($pattern);
        $this->name = $name;
        $this->controllerAction = $action;
        $this->controllerClass = $controller;
    }

    /**
     * Determines if the current request matches this route
     *
     * @param Request $request
     *
     * @return bool
     */
    public function matches( Request $request )
    {
        $requestParts = $request->getPathParts();
        if (count($requestParts) !== count($this->parts)) {
            return false;
        }
        if (!in_array($request->method(), $this->types)) {
            return false;
        }

        foreach($requestParts as $index => $requestPart) {
            $part = $this->parts[$index];
            if (is_object($part) && $part instanceof RouteParameter) {
                if (!$part->matches($requestPart)) {
                    return false;
                }
            } else if ($requestPart !== $part) {
                return false;
            }
        }

        return true;
    }

    /**
     * Returns the controller class name for this route
     *
     * @return string
     */
    public function getControllerClass()
    {
        return $this->controllerClass;
    }

    /**
     * Returns the name of the controller method for this route
     *
     * @return string
     */
    public function getControllerAction()
    {
        return $this->controllerAction;
    }

    /**
     * Returns an array of key value pairs of parameters from the current request
     *
     * This method should only be called after matches has been called and returns true
     *
     * @param Request $request
     *
     * @return string[]
     */
    public function resolveParameters( Request $request )
    {
        $parameters = array();

        $requestParts = $request->getPathParts();
        if (count($requestParts) !== count($this->parts)) {
            return $parameters;
        }

        foreach($requestParts as $index => $requestPart) {
            $part = $this->parts[$index];
            if (is_object($part) && $part instanceof RouteParameter) {
                $parameters[$part->name] = $requestPart;
            }
        }

        return $parameters;
    }

    /**
     * Returns a uri path for this route utilizing the giving params array
     *
     * @param array $params The parameters used to generate the route
     *
     * @return string|null null if the parameters are not sufficient to generate a path
     */
    public function getPath( $params = array() )
    {
        $path = "";

        foreach($this->parts as $part) {
            if (is_object($part) && $part instanceof RouteParameter) {
                if (isset($params[$part->name])) {
                    $path .= "/" . $params[$part->name];
                } else {
                    return null;
                }
            } else {
                $path .= "/" . $part;
            }
        }

        return $path === "" ? "/" : $path;
    }

    /**
     * Returns the base part of the route's path (the part before the parameters)
     *
     * @return string
     */
    public function getBasePath()
    {
        $path = "";
        foreach($this->parts as $part) {
            if (is_object($part)) {
                break;
            }

            $path .= "/" . $part;
        }

        return $path === "" ? "/" : $path;
    }

    /**
     * Adds constraints to a route's parameters
     *
     * @param array $constraints An associative array of constraints where the keys are the route parameters and the value is regex or keyword
     *                           Accepted keywords are "int" "integer" "float" "double"
     *
     * @return void
     */
    public function constraints(array $constraints)
    {
        foreach($this->parts as $part) {
            if (is_object($part) && $part instanceof RouteParameter && isset($constraints[$part->name])) {
                $part->addConstraint($constraints[$part->name]);
            }
        }
    }


    /**
     * Parses a pattern for matching later
     *
     * @param string $pattern
     *
     * @return array|(string|RouteParameter)[]
     */
    protected function parsePattern( $pattern )
    {
        // Get rid of leading and trailing slashes
        $pattern = trim($pattern, "/");

        // Split the pattern into its parts
        $parts = explode("/", $pattern);

        /** @type (string|RouteParameter)[] $result */
        $result = array();

        foreach($parts as $part) {
            // Check for a parameter
            if (strlen($part) > 1 && $part[0] === ':') {
                $result[] = new RouteParameter(substr($part, 1));
            } else {
                $result[] = $part;
            }
        }

        return $result;
    }
}