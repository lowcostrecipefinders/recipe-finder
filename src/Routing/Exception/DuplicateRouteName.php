<?php

namespace RecipeFinders\Routing\Exception;

class DuplicateRouteName extends \Exception
{
    public function __construct($routeName)
    {
        parent::__construct(sprintf("Found two routes with the same name '%s'.", $routeName));
    }
}