<?php

namespace RecipeFinders\Routing\Exception;

class MissingController extends \Exception
{
    public function __construct($routeName)
    {
        parent::__construct(sprintf("A route with the name '%s' is missing a controller.", $routeName));
    }
}