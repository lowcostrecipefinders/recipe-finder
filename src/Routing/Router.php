<?php

namespace RecipeFinders\Routing;

use RecipeFinders\DependencyInjection\Container;
use RecipeFinders\DependencyInjection\ContainerAware;
use RecipeFinders\Http\Request;
use RecipeFinders\Http\Response;
use RecipeFinders\Routing\Exception\DuplicateRouteName;
use RecipeFinders\Routing\Exception\MissingController;

/**
 * Class Router
 *
 * The router is responsible for finding the controller and action for a request
 *
 * @package RecipeFinders\Routing
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Router implements ContainerAware
{

    /** @type string NAME_SEPARATOR The separator between name parts when grouping routs */
    const NAME_SEPARATOR = "_";

    /** @type Container $container */
    protected $container;

    /** @type string[] $nameStack  A stack of names used when grouping routes */
    protected $nameStack = array();

    /** @type string[] $controllerStack  A stack of class names used when grouping routes */
    protected $controllerStack = array();

    /** @type string[] $patternStack  A stack of patterns used when grouping routes */
    protected $patternStack = array();

    /** @type Route[] $routes  A collection of all the routes for the router */
    protected $routes = array();

    /**
     * Constructor
     */
    public function __construct()
    {
    }

    /**
     * Finds the correct route, and calls the routing method
     *
     * @param Request $request
     *
     * @return false|string|Response
     */
    public function run(Request $request)
    {
        foreach($this->routes as $route) {
            if ($route->matches($request)){

                // Create the controller
                $controller = $this->container->createObject($route->getControllerClass());
                if (is_null($controller)) {
                    continue;
                }
                $result = $this->container->callObjectMethod($controller, $route->getControllerAction(), $route->resolveParameters($request));
                if ($result !== false) {
                    return $result;
                }
            }
        }

        return false;
    }

    /**
     * Creates a route and adds it to the routing collection
     *
     * @param string|string[] $types      The request types that match this request (i.e. "query", "request", "put")
     * @param string          $pattern    A pattern to match the url with
     * @param string          $name       A name for the route that can be used to reference it later
     * @param string          $action     The name of the method of the controller to use for this route
     * @param string          $controller The fully qualified class name for the controller to use for the request
     *                                    This controller parameter can be empty only if contained within a group
     *
     * @return Route
     * @throws MissingController  When a controller can not be resolved for the route
     * @throws DuplicateRouteName When a route already exists with this routes name
     */
    public function route($types, $pattern, $name, $action, $controller = '')
    {
        if (!is_array($types)) {
            $types = array($types);
        }

        $name = empty($this->nameStack) ? $name : implode(self::NAME_SEPARATOR, $this->nameStack) . self::NAME_SEPARATOR . $name;
        $pattern = empty($this->patternStack) ? $pattern : implode("", $this->patternStack) . $pattern;
        $controller = !($controller) ? end($this->controllerStack) : $controller;

        if (!$controller) {
            throw new MissingController($name);
        }

        $route = new Route($types, $pattern, $name, $action, $controller);

        if (isset($this->routes[$name])) {
            throw new DuplicateRouteName($name);
        }

        $this->routes[$name] = $route;

        return $route;
    }

    /**
     * Shortcut method for adding a query request route
     *
     * @param string          $pattern    A pattern to match the url with
     * @param string          $name       A name for the route that can be used to reference it later
     * @param string          $action     The name of the method of the controller to use for this route
     * @param string          $controller The fully qualified class name for the controller to use for the request
     *                                    This controller parameter can be empty only if contained within a group
     * @return Route
     */
    public function get($pattern, $name, $action, $controller = '')
    {
        return $this->route('get', $pattern, $name, $action, $controller);
    }

    /**
     * Shortcut method for adding a request request route
     *
     * @param string          $pattern    A pattern to match the url with
     * @param string          $name       A name for the route that can be used to reference it later
     * @param string          $action     The name of the method of the controller to use for this route
     * @param string          $controller The fully qualified class name for the controller to use for the request
     *                                    This controller parameter can be empty only if contained within a group
     * @return Route
     */
    public function post($pattern, $name, $action, $controller = '')
    {
        return $this->route('post', $pattern, $name, $action, $controller);
    }

    /**
     * Shortcut method for adding a put request route
     *
     * @param string          $pattern    A pattern to match the url with
     * @param string          $name       A name for the route that can be used to reference it later
     * @param string          $action     The name of the method of the controller to use for this route
     * @param string          $controller The fully qualified class name for the controller to use for the request
     *                                    This controller parameter can be empty only if contained within a group
     * @return Route
     */
    public function put($pattern, $name, $action, $controller = '')
    {
        return $this->route('put', $pattern, $name, $action, $controller);
    }

    /**
     * Shortcut method for adding a delete request route
     *
     * @param string          $pattern    A pattern to match the url with
     * @param string          $name       A name for the route that can be used to reference it later
     * @param string          $action     The name of the method of the controller to use for this route
     * @param string          $controller The fully qualified class name for the controller to use for the request
     *                                    This controller parameter can be empty only if contained within a group
     * @return Route
     */
    public function delete($pattern, $name, $action, $controller = '')
    {
        return $this->route('delete', $pattern, $name, $action, $controller);
    }

    /**
     * A convenience method for grouping routes.
     *
     * @param string       $pattern     A base pattern for this group
     * @param string       $name        A base name for this group
     * @param string|false $controller  The class name for a default controller for this group, or false to use a higher nested groups
     * @param callable     $grouping    An anonymous function to call to define the routes (and subgroups) within the group
     *
     * @return void
     */
    public function group($pattern, $name, $controller, callable $grouping)
    {
        // Add items to stacks
        array_push($this->patternStack, $pattern);
        array_push($this->nameStack, $name);
        if ($controller) {
            array_push($this->controllerStack, $controller);
        }

        // Call the grouping function
        $grouping();

        // Pop the stacks
        array_pop($this->patternStack);
        array_pop($this->nameStack);
        if ($controller) {
            array_pop($this->controllerStack);
        }
    }

    /**
     * Returns a route with the specified name
     *
     * @param string $name The name of the route being searched for
     *
     * @return Route|null  null if a route with the given name does not exist
     */
    public function getRouteByName($name)
    {
        return isset($this->routes[$name]) ? $this->routes[$name] : null;
    }

    /**
     * Returns a path to a given route with the specified name (for a link)
     *
     * @param string $name The name of the route to query a path for
     * @param array  $params An associative array of parameters to complete the route
     *
     * @return null|string null if the route does not exist or if the parameters given were not sufficient
     */
    public function getPathForRoute($name, $params = array())
    {
        $route = $this->getRouteByName($name);
        return is_null($route) ? null : $route->getPath($params);
    }

    /**
     * Gets a full url for a route with the given parameters
     *
     * @param string $name The name of the route to generate a url for
     * @param array  $params
     * @return null|string
     */
    public function getUrlForRoute($name, $params = array())
    {
        /** @type Request $request */
        $request = $this->container->get('request');
        $path = $this->getPathForRoute($name, $params);
        return is_null($path) ? null : $request->getBaseUrl() . $path;
    }

    /**
     * Set the container in the object
     *
     * @param Container $container The container object that will be injected
     *
     * @return void
     */
    public function setContainer( Container $container )
    {
        $this->container = $container;
    }

    /**
     * Recursively loads routing files from a directory
     *
     * @param string $routeDirectory The path to the routing file
     */
    public function loadRoutesFromDirectory( $routeDirectory )
    {
        // Used in the routing files
        $router = $this;

        foreach (scandir($routeDirectory) as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            $path = $routeDirectory . DIRECTORY_SEPARATOR . $file;

            if (is_dir($path)) {
                $this->loadRoutesFromDirectory($path);
            } else {
                include($path);
            }
        }

    }


}