<?php

namespace RecipeFinders\Routing;

/**
 * Class RouteParameter
 *
 * A route parameter is a uri parameter that will be passed to a controller based on the uri.
 *
 * It exists as a separate class in order to facilitate testing of constraints on the parameter
 *
 * @package RecipeFinders\Routing
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class RouteParameter
{
    /** @type string $name The name of the route parameter that will be passed to resolve controller arguments */
    public $name;

    /** @type string[] $constraints A list of regex/keyword constraints used to test for matches with the uri */
    public $constraints = array();

    /**
     * Constructor
     *
     * @param string $name The name of the parameter
     */
    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Adds a constraint to the parameter
     *
     * @param string|string[] $constraint A constraint is a regex pattern, or any of the key words "int", "integer", "float", "double"
     *
     * @return void
     */
    public function addConstraint($constraint)
    {
        if (!is_array($constraint)) {
            $constraint = array($constraint);
        }

        $this->constraints = array_merge($this->constraints, $constraint);
    }

    /**
     * Determines if the uri part this parameter is for matches the given input
     *
     * @param string $data The value from the part of the uri where this route parameter is at
     *
     * @return bool
     */
    public function matches($data)
    {
        foreach($this->constraints as $constraint) {
            switch ($constraint) {
                case "int":
                case "integer":
                    $regex = '/^\d+$/';
                    break;
                case "float":
                case "double":
                    $regex = '/^\d+\.?\d*$/';
                    break;
                default:
                    $regex = $constraint;
            }

            // preg_match returns 0 for no match, 1 for a match, and false when error
            if (!preg_match($regex, $data)) {
                return false;
            }
        }

        return true;
    }
}