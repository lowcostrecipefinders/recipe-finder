<?php

namespace RecipeFinders\Measurements;

/**
 * Class Units
 *
 * A container for holding constant values for units of measurements
 *
 * Static functions to detect units from strings could be added if we need it
 *
 * @package RecipeFinders\Measurements
 * @author  Mark Wardle <wardlem@onid.oregonstate.edu>
 */
class Units
{

    // VOLUME CONSTANTS
    const TEASPOON = 'tsp';
    const TABLESPOON = 'tbsp';
    const FLUID_OUNCES = 'fl oz';
    const CUP = 'cup';
    const PINT = 'pt';
    const QUART = 'qt';
    const GALLON = 'gal';
    const MILLILITER = 'mL';
    const LITER = 'L';
    const DECILITER = 'dL';

    // MASS AND WEIGHT CONSTANTS
    const POUND = 'lb';
    const OUNCE = 'oz';
    const MILLIGRAM = 'mg';
    const GRAM = 'g';
    const KILOGRAM = 'kg';

    // LENGTH CONSTANTS
    const MILLIMETER = 'mm';
    const CENTIMETER = 'cm';
    const METER = 'm';
    const INCH = 'in';
    const FOOT = 'ft';
    const YARD = 'y';

    // GENERIC CONSTANTS
    const WHOLE = 'whole';
    const PIECE = 'piece';

    protected static $unitNames = array(
        // VOLUME

        // Teaspoon
        'teaspoon' => self::TEASPOON,
        'tsp' => self::TEASPOON,
        't' => self::TEASPOON,

        // Tablespoon
        'tablespoon' => self::TABLESPOON,
        'T' => self::TABLESPOON,
        'tbl' => self::TABLESPOON,
        'tb' => self::TABLESPOON,
        'tbsp' => self::TABLESPOON,

        // Fluid Ounces
        'fluid ounce' => self::FLUID_OUNCES,
        'fl oz' => self::FLUID_OUNCES,

        // Cups
        'cup' => self::CUP,
        'c' => self::CUP,

        // Pint
        'pint' => self::PINT,
        'p' => self::PINT,
        'pt' => self::PINT,
        'fl pt' => self::PINT,

        // Quart
        'quart' => self::QUART,
        'q' => self::QUART,
        'qt' => self::QUART,
        'fl qt' => self::QUART,

        // Gallon
        'gallon' => self::GALLON,
        'gal' => self::GALLON,

        // Milliliter
        'ml' => self::MILLILITER,
        'milliliter' => self::MILLILITER,
        'millilitre' => self::MILLILITER,
        'cc' => self::MILLILITER,

        // Liter
        'l' => self::LITER,
        'liter' => self::LITER,
        'litre' => self::LITER,

        // Deciliter
        'dl' => self::DECILITER,
        'deciliter' => self::DECILITER,
        'decilitre' => self::DECILITER,

        // WEIGHTS

        // Pound
        'pound' => self::POUND,
        'lb' => self::POUND,
        '#' => self::POUND,

        // Ounce
        'ounce' => self::OUNCE,
        'oz' => self::OUNCE,

        // Milligram
        'mg' => self::MILLIGRAM,
        'milligram' => self::MILLIGRAM,
        'milligramme' => self::MILLIGRAM,

        // Gram
        'g' => self::GRAM,
        'gram' => self::GRAM,
        'gramme' => self::GRAM,

        // Kilogram
        'kg' => self::KILOGRAM,
        'kilogram' => self::KILOGRAM,
        'kilogramme' => self::KILOGRAM,

        // Pieces
        'piece' => self::PIECE,
        'pc' => self::PIECE,

        // Whole
        'whole' => self::WHOLE,

        // LENGTHS

        // millimeter
        'millimeter' => self::MILLIMETER,
        'mm' => self::MILLIMETER,

        // centimeter
        'centimeter' => self::CENTIMETER,
        'cm' => self::CENTIMETER,

        // meter
        'meter' => self::METER,
        'm' => self::METER,

        // inch
        'inch' => self::INCH,
        'in' => self::INCH,

        // foot
        'foot' => self::FOOT,
        'ft' => self::FOOT,

        // yard
        'yard' => self::YARD,
        'y' => self::YARD

    );

    /**
     * Takes a string for a measurement unit and determines the canonical unit name
     *
     * @param  string $string
     * @return string
     */
    public static function detectUnitFromString($string)
    {
        // Condense white space
        $string =  preg_replace('!\s+!', ' ', strtolower(trim($string, 's.')));

        $parts = explode(' ', $string);
        $parts = array_map(function($el) {return rtrim($el, ',.s');}, $parts);

        // See if there is a double word that maps to a key
        foreach ($parts as $index => $part) {
            if ($index + 1 >= count($parts)) {
                break;
            }
            $test = $part . ' ' . $parts[$index + 1];
            if (isset(self::$unitNames[$test])) {
                return self::$unitNames[$test];
            }
        }

        // See if there is a single word that maps to a key
        foreach($parts as $test) {
            if (isset(self::$unitNames[$test])) {
                return self::$unitNames[$test];
            }
        }

        return self::PIECE;
    }

    public static function isUnit($unit)
    {

        return
            self::isVolumeUnit($unit) ||
            self::isWeightUnit($unit) ||
            self::isLengthUnit($unit) ||
            self::isGenericUnit($unit);

    }

    public static function isVolumeUnit($unit)
    {
        return
            $unit === self::TEASPOON ||
            $unit === self::TABLESPOON ||
            $unit === self::FLUID_OUNCES ||
            $unit === self::CUP ||
            $unit === self::PINT ||
            $unit === self::QUART ||
            $unit === self::GALLON ||
            $unit === self::MILLILITER ||
            $unit === self::LITER ||
            $unit === self::DECILITER;
    }

    public static function isWeightUnit($unit)
    {
        return
            $unit === self::POUND ||
            $unit === self::OUNCE ||
            $unit === self::MILLIGRAM ||
            $unit === self::GRAM ||
            $unit === self::KILOGRAM;
    }

    public static function isLengthUnit($unit)
    {
        return
            $unit === self::MILLIMETER ||
            $unit === self::CENTIMETER ||
            $unit === self::METER ||
            $unit === self::INCH ||
            $unit === self::FOOT ||
            $unit === self::YARD;

    }

    public static function isGenericUnit($unit)
    {
        return
            $unit === self::WHOLE ||
            $unit === self::PIECE;
    }

}