<?php

namespace RecipeFinders\Measurements;
/**
 * Class Converter
 *
 * The converter class provides methods for converting between units of measurements
 *
 * @package RecipeFinders\RecipeRanker\Measurements
 */
class Converter
{
    /**
     * VOLUME CONVERSION CONSTANTS
     *
     * measured in grams
     *
     * weights are for the given volume of water, rounded up
     *
     */

    const TEASPOON_WEIGHT = 5;
    const TABLESPOON_WEIGHT = 15;
    const FLUID_OUNCES_WEIGHT = 30;
    const CUP_WEIGHT = 238;
    const PINT_WEIGHT = 475;
    const QUART_WEIGHT = 950;
    const GALLON_WEIGHT = 3800;
    const MILLILITER_WEIGHT = 1;
    const LITER_WEIGHT = 1000;
    const DECILITER_WEIGHT = 380;

    /**
     * WEIGHT CONVERSION CONSTANTS
     *
     * measured in grams to 2 decimal places
     *
     */

    const POUND_WEIGHT = 453.59;
    const OUNCE_WEIGHT = 28.35;
    const MILLIGRAM_WEIGHT = 0.001;
    const GRAM_WEIGHT = 1;
    const KILOGRAM_WEIGHT = 1000;

    /**
     * LENGTH CONVERSION CONSTANTS
     *
     * measured in grams
     *
     * using one cubic inch of water for every inch in length
     * results in 16.5 grams per inch of length
     */

    const MILLIMETER_WEIGHT = 0.65;
    const CENTIMETER_WEIGHT = 6.50;
    const METER_WEIGHT = 422.0;
    const INCH_WEIGHT = 16.5;
    const FOOT_WEIGHT = 198.0;
    const YARD_WEIGHT = 594.0;

    /**
     * ABSTRACT CONVERSION CONSTANTS
     *
     * measured in grams
     *
     */

    const DEFAULT_WEIGHT = 100;
    const WHOLE_WEIGHT = 1000;
    const PIECE_WEIGHT = 200;


    /**
     * Constructor
     */
    public function __construct()
    {
    }



    /**
     * Converts one unit of measurement to another
     *
     * @param string    $fromUnitOfMeasurement  The unit of measurement converting from
     *                                          These should be taken from constants
     * @param float|int $fromQuantity           The quantity to be converted
     * @param string    $toUnitOfMeasurement    The unit of measurement converting to
     *                                          These should be taken from constants
     *
     * @return float    The converted quantity
     */
    public function convert($fromUnitOfMeasurement, $fromQuantity, $toUnitOfMeasurement)
    {
        if (
            !Units::isUnit($fromUnitOfMeasurement) ||
            !(Units::isVolumeUnit($toUnitOfMeasurement) ||
                Units::isWeightUnit($toUnitOfMeasurement)) ||
            $fromQuantity <= 0) {
            return 0;
        }

        return $this->doConvert($fromUnitOfMeasurement, $fromQuantity, $toUnitOfMeasurement);
    }

    /**
     * @param string $fromUnitOfMeasurement
     * @param float|int $fromQuantity
     * @param string $toUnitOfMeasurement
     * @return float
     */
    protected function doConvert($fromUnitOfMeasurement, $fromQuantity, $toUnitOfMeasurement)
    {
        if ($fromUnitOfMeasurement === $toUnitOfMeasurement) {
            return (float)$fromQuantity;
        }

        switch($fromUnitOfMeasurement) {
            case Units::GRAM:
                return $this->convertGrams($fromQuantity, $toUnitOfMeasurement);
            case Units::LITER:
                return $this->convertLiters($fromQuantity, $toUnitOfMeasurement);
            case Units::GALLON:
                return $this->convert(Units::LITER, $fromQuantity / 0.264, $toUnitOfMeasurement);
            case Units::CUP:
                return $this->convert(Units::GALLON, $fromQuantity / 16.0, $toUnitOfMeasurement);
            case Units::TEASPOON:
                return $this->convert(Units::CUP, $fromQuantity / (3.0 * 16.0), $toUnitOfMeasurement);
            case Units::TABLESPOON:
                return $this->convert(Units::CUP, $fromQuantity / 16.0, $toUnitOfMeasurement);
            case Units::FLUID_OUNCES:
                return $this->convert(Units::CUP, $fromQuantity / 8.0, $toUnitOfMeasurement);
            case Units::PINT:
                return $this->convert(Units::GALLON, $fromQuantity / 8.0, $toUnitOfMeasurement);
            case Units::QUART:
                return $this->convert(Units::GALLON, $fromQuantity / 4.0, $toUnitOfMeasurement);
            case Units::MILLILITER:
                return $this->convert(Units::LITER, $fromQuantity / 1000.0, $toUnitOfMeasurement);
            case Units::DECILITER:
                return $this->convert(Units::LITER, $fromQuantity / 10.0, $toUnitOfMeasurement);
            case Units::POUND:
                return $this->convert(Units::GRAM, $fromQuantity * self::POUND_WEIGHT, $toUnitOfMeasurement);
            case Units::OUNCE:
                return $this->convert(Units::GRAM, $fromQuantity * self::OUNCE_WEIGHT, $toUnitOfMeasurement);
            case Units::KILOGRAM:
                return $this->convert(Units::GRAM, $fromQuantity * 1000.0, $toUnitOfMeasurement);
            case Units::MILLIGRAM:
                return $this->convert(Units::GRAM, $fromQuantity * (1.0/1000.0), $toUnitOfMeasurement);
            case Units::PIECE:
                return $this->convert(Units::GRAM, $fromQuantity * self::PIECE_WEIGHT, $toUnitOfMeasurement);
            case Units::WHOLE:
                return $this->convert(Units::GRAM, $fromQuantity * self::WHOLE_WEIGHT, $toUnitOfMeasurement);
            case Units::CENTIMETER:
                return $this->convert(Units::GRAM, $fromQuantity * self::CENTIMETER_WEIGHT, $toUnitOfMeasurement);
            case Units::MILLIMETER:
                return $this->convert(Units::GRAM, $fromQuantity * self::MILLIMETER_WEIGHT, $toUnitOfMeasurement);
            case Units::METER:
                return $this->convert(Units::GRAM, $fromQuantity * self::METER_WEIGHT, $toUnitOfMeasurement);
            case Units::INCH:
                return $this->convert(Units::GRAM, $fromQuantity * self:: INCH_WEIGHT, $toUnitOfMeasurement);
            case Units::FOOT:
                return $this->convert(Units::GRAM, $fromQuantity * self:: FOOT_WEIGHT, $toUnitOfMeasurement);
            case Units::YARD:
                return $this->convert(Units::GRAM, $fromQuantity * self:: YARD_WEIGHT, $toUnitOfMeasurement);


        }

        return $this->convert(Units::GRAM, self::DEFAULT_WEIGHT, $toUnitOfMeasurement);
    }

    private function convertLiters($fromQuantity, $toUnitOfMeasurement)
    {

        switch($toUnitOfMeasurement) {
            case Units::LITER:
                return $fromQuantity;
            case Units::GALLON:
                return $this->convertLiters($fromQuantity * 0.264, Units::LITER);
            case Units::CUP:
                return $this->convertLiters($fromQuantity * 16.0, Units::GALLON);
            case Units::TEASPOON:
                return $this->convertLiters($fromQuantity * (3.0 * 16.0), Units::CUP);
            case Units::TABLESPOON:
                return $this->convertLiters($fromQuantity * 16.0, Units::CUP);
            case Units::FLUID_OUNCES:
                return $this->convertLiters($fromQuantity * 8.0, Units::CUP);
            case Units::PINT:
                return $this->convertLiters($fromQuantity * 8.0, Units::GALLON);
            case Units::QUART:
                return $this->convertLiters($fromQuantity * 4.0, Units::GALLON);
            case Units::MILLILITER:
                return $this->convertLiters($fromQuantity * 1000.0, Units::LITER);
            case Units::DECILITER:
                return $this->convertLiters($fromQuantity * 10.0, Units::LITER);
        }

        // Must be weight
        return $this->convert(Units::GRAM, $fromQuantity * self::LITER_WEIGHT, $toUnitOfMeasurement);
    }

    private function convertGrams($fromQuantity, $toUnitOfMeasurement)
    {

        switch ($toUnitOfMeasurement) {
            case Units::GRAM:
                return $fromQuantity;
            case Units::MILLIGRAM:
                return $fromQuantity * 1000.0;
            case Units::KILOGRAM:
                return $fromQuantity / 1000.0;
            case Units::POUND:
                return $this->convertGrams($fromQuantity * 2.2, Units::KILOGRAM);
            case Units::OUNCE:
                return $this->convertGrams($fromQuantity * 16, Units::POUND);
        }

        // Must be volume
        return $this->convert(Units::LITER, $fromQuantity / self::LITER_WEIGHT, $toUnitOfMeasurement);
    }
}