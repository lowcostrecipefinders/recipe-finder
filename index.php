<?php

/**
 * This file acts as the front controller to all web requests in the application.
 *
 * It performs necessary initialization procedures, including:
 *   1. Sets error reporting
 *   2. Sets up autoloading
 *   3. Sets up the service container with shared services
 *   4. Creates a router, determining a route, and calling a route function
 *
 * @author Mark Wardle <wardlem@onid.oregonstate.edu>
 */

use RecipeFinders\DependencyInjection\Container;
use RecipeFinders\Http\Request;
use RecipeFinders\Http\Response;
use RecipeFinders\Http\Session;
use RecipeFinders\Routing\Router;
use RecipeFinders\Util\Environment;
use RecipeFinders\View\View;

// Set error level
error_reporting(E_ALL);

// Catch output in the buffer
// This prevents stray echo statements from messing up the headers
ob_start();

function getRootPath() {
    $self = $_SERVER['PHP_SELF'];
    $root = str_replace('/index.php', '', $self);
    return $root;
}


// Set up the autoloader
require_once __DIR__ . "/vendor/autoload.php";

// Create the dependency injection container
/** @type Container $container */
$container = new \RecipeFinders\DependencyInjection\Container(true);

// Create the request object
/** @type Request $request */
$request = $container->createObject("RecipeFinders\\Http\\Request", array('rootPath' => getRootPath()));
$container->share($request, 'request');

// Set up the environment
/** @type Environment $environment */
$environment = $container->createObject("RecipeFinders\\Util\\Environment");
$container->share($environment, 'environment');

try {

    // Load configuration
    $configFile = __DIR__ . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config.php";
    $envConfigFile = __DIR__ . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "config_" . $environment->environmentName() . ".php";
    if (file_exists($configFile)){
        include($configFile);
    }
    if (file_exists($envConfigFile)){
        include($envConfigFile);
    }

    $environment->setValues(array('document_root' => __DIR__));

    // Prep the view object
    $container->share(function() use ($container) {
        /** @type View $view */
        $view = $container->createObject("RecipeFinders\\View\\View");
        $view->loadHelpersFromFile(__DIR__ . "/config/view_helpers.php");
        return $view;
    }, "RecipeFinders\\View\\View", "view");

    // Create the session object
    /** @type Session $session */
    $session = $container->createObject("RecipeFinders\\Http\\Session", array('cycleFlash' => true));
    $container->share($session, 'session');

    // Initialize the session and cycle flash values
    $session->startSession(true);

    // Create Router
    /** @type Router $router */
    $router = $container->createObject("RecipeFinders\\Routing\\Router");
    $container->share($router, 'router');

    // Load the routing configuration files
    $routeDirectory = __DIR__ . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "routes";
    $router->loadRoutesFromDirectory($routeDirectory);

    $mysqliFactory = function() use ($container, $environment) {
        $host = $environment->get('db.host');
        $user = $environment->get('db.username');
        $pass = $environment->get('db.password');
        $name = $environment->get('db.name');
        $port = $environment->get('db.port');
        $mysqli = new \mysqli($host, $user, $pass, $name, $port);
        if ($mysqli->connect_errno) {
            die($mysqli->connect_error);
        }
        return $mysqli;
    };
    $container->share($mysqliFactory, "mysqli");

    // Add the recipe finder to the container
    $recipeFinderFactory = function() use ($container) {
        $recipeFinder = new \RecipeFinders\RecipeFinder\RecipeFinder();
        return $recipeFinder;
    };
    $container->share($recipeFinderFactory,"RecipeFinders\\RecipeFinder\\RecipeFinder", "recipe_finder");
    $container->addAlias("RecipeFinders\\Model\\RecipeFinderInterface", "recipe_finder");

    // Add the ingredient pricer to the container
    $ingredientPricerFactory = function() use ($container) {
        $mysqli = $container->get('mysqli');
        $ingredientPricer = new \RecipeFinders\IngredientPricer\IngredientPricer($mysqli);
        return $ingredientPricer;
    };
    $container->share($ingredientPricerFactory,"RecipeFinders\\Model\\IngredientPricerInterface", "ingredient_pricer");

    $converterFactory = function() use ($container) {
        $converter = new \RecipeFinders\Measurements\Converter();
        return $converter;
    };

    $container->share($converterFactory,"RecipeFinders\\Measurements\\Converter", "measurement_converter");


    // Add the recipe ranker to the container
    $recipeRankerFactory = function() use ($container) {
        echo '<p>Making ranker</p>';

        $pricer = $container->get('ingredient_pricer');
        $converter = $container->get('measurement_converter');
        $ranker = new \RecipeFinders\RecipeRanker\RecipeRanker($pricer, $converter);

        echo '<p>Type of ranker: ' . get_class($ranker) .'</p>';

        return $ranker;
    };
    $container->share($recipeRankerFactory, "RecipeFinders\\RecipeRanker\\RecipeRanker", "recipe_ranker");
    $container->addAlias("RecipeFinders\\Model\\RecipeRankerInterface", "recipe_ranker");

    // Run the router
    $response = $router->run($request);

    if ($response === false) {
        $response = new Response("<p>This page does not exist</p>", 404);
    } else if (is_string($response)){
        $response = new Response($response);
    }

    if (!($response instanceof Response)) {
        if ($environment->isLocal()) {
            throw new \RecipeFinders\Http\Exception\InvalidResponse();
        }
    }

    $response->flush();

    // Flush the output buffer
    ob_flush();
    ob_clean();

} catch (\Exception $e) {
    if ($environment->isLocal() || true) {
        // Print a stack trace
        echo "<h2>Uncaught Exception</h2>";
        do {
            echo "<div>";
            printf("<p style='margin-bottom:0'>%s (line: %d)\n</p>", $e->getFile(), $e->getLine());
            printf("<p style='margin-top:0; padding-left:1em;'>%s: %s\n</p>", get_class($e), $e->getMessage());
            echo "</div>";
        } while ($e = $e->getPrevious());
    } else {
        // Show that there was an error
        echo "<p>An error occurred. Sorry.</p>";
    }

    // Would go in a finally if using php 5.5
    // Is this the right place for this?
    ob_flush();
    ob_clean();
}


