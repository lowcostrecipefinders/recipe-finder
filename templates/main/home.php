<!DOCTYPE html>
<html>
    <head>
	<title> Low-Cost Recipe Finder </title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="recipe.css">
    </head>
    <body>
    <div id="title">
	<h1> Low-Cost Recipe Finder </h1>
    </div>
    <div id="header">
	<img src="recipe.jpg" alt="Recipe">
    </div>
    <div id="form-wrapper">
    	<form method="get" action="search">
        	<label>Search:
            	<input type="text" name="q">
        	</label>
    	</form>
    </div>
    </body>
</html>
