<!DOCTYPE html>
<html>
<head>
	<title> Search Results </title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" href="recipe.css">
</head>
<body>
    <div id="title">
    	<h1>Search results</h1>
    </div>
</body>
</html>
    <?php if (count($recipes) < 1): ?>
        <div id="results"> <p>Your search returned no results.</p></div>
    <?php else: ?>
        <ol>
            <?php foreach($recipes as $recipe): ?>
                <li>
                    <div>
                        <h5><?php echo $recipe->getName() ?></h5>
                        <ul>
                            <li>Rank: <?php echo $recipe->getRank(); ?></li>
                            <li>Cost Per Serving: <?php echo number_format($recipe->getCostPerPersonFed(), 2) ?></li>
                            <li>Servings: <?php echo $recipe->getServings() ?></li>
                        </ul>
                    </div>
                </li>
            <?php endforeach; ?>
        </ol>
    <?php endif ?>
