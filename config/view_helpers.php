<?php
use RecipeFinders\View\View;

if (!isset($view) || !($view instanceof View)) {
    return; // die?
}

/** @type View $view */
$view->addHelper('test', function() {
    return 'Beuller??';
});