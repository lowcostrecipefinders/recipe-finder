<?php

use RecipeFinders\Util\Environment;

if (!isset($environment) || !($environment instanceof Environment)) {
    return; //die ?
}

$settings = array(
    'testSetting1' => 'Number 1'
);

$environment->setValues($settings);