<?php

use RecipeFinders\Routing\Router;

if (!isset($router) || !($router instanceof Router)) {
    return;  // or die?
}

// Route definitions here

$router->get("/", "home", 'homeAction', "RecipeFinders\\Controller\\SearchController");
$router->get("/search", "search", 'searchAction', "RecipeFinders\\Controller\\SearchController");
