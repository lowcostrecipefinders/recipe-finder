<?php

namespace RecipeFinders\Test\Routing;

use PHPUnit_Framework_TestCase as Test;
use RecipeFinders\Routing\Router;
use RecipeFinders\Test\MockFactory;

class RouterTest extends Test
{
    public function testRouter()
    {
        $container = MockFactory::getContainer(array('root_path' => ''));

        $this->assertInstanceOf("RecipeFinders\\DependencyInjection\\Container", $container);

        /** @type Router $router */
        $router = $container->get('router');

        $this->assertInstanceOf("RecipeFinders\\Routing\\Router", $router);

        $router->get('/', 'home', 'indexAction', 'RecipeFinders\\Controller\\HomeController');

        $route = $router->getRouteByName('home');

        $this->assertInstanceOf("RecipeFinders\\Routing\\Route", $route);



    }
}