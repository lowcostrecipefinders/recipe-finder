<?php

namespace RecipeFinders\Test\RecipeFinder;

use PHPUnit_Framework_TestCase as Test;
use RecipeFinders\RecipeFinder\RecipeFinder;

class RecipeFinderTest extends Test
{
    public function testRecipeFinder()
    {


        $finder = new RecipeFinder();

        $foundRecipes = $finder->findRecipes('bread');

        // use this to print out api response
        // var_dump($foundRecipes);
        
        $this->assertNotEmpty($foundRecipes);

        foreach($foundRecipes as $recipe){

            $this->assertNotEmpty($recipe->getName());
            $this->assertNotEmpty($recipe->getIngredients());
            $this->assertNotEmpty($recipe->getId());
            $this->assertNotEmpty($recipe->getServings());
        }

    }
}
