<?php

namespace RecipeFinders\Test;

use RecipeFinders\Test\DependencyInjection\ContainerMock;

class MockFactory
{
    static function getContainer(array $options)
    {
        $container = new ContainerMock();
        $request = $container->createObject("RecipeFinders\\Test\\Http\\RequestMock", array('rootPath' => $options['root_path']));
        $container->share($request, 'request');

        $environment = $container->createObject("RecipeFinders\\Util\\Environment");
        $container->share($environment, 'environment');

        $session = $container->createObject("RecipeFinders\\Http\\Session", array('cycleFlash' => false));
        $container->share($session, 'session');

        $router = $container->createObject("RecipeFinders\\Routing\\Router");
        $container->share($router, 'router');

        return $container;
    }
}