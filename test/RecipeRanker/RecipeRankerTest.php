<?php

namespace RecipeFinders\Test\RecipeRanker;

use PHPUnit_Framework_TestCase as Test;
use RecipeFinders\Measurements\Units;
use RecipeFinders\RecipeRanker\RecipeRanker;
use RecipeFinders\Test\RecipeRanker\Model\MockIngredient;
use RecipeFinders\Test\RecipeRanker\Model\MockRecipe;

class RecipeRankerTest extends Test
{
    public function testRecipeRanker()
    {

        $recipe1 = new MockRecipe('1', 'Recipe 1', 2, array(
            new MockIngredient('chicken', 1, Units::POUND)
        ));

        $recipe2 = new MockRecipe('2', 'Recipe 2', 2, array(
            new MockIngredient('chicken', 16, Units::OUNCE),
            new MockIngredient('Thyme', 1, Units::TABLESPOON)
        ));

        $recipe3 = new MockRecipe('3', 'Recipe 3', 1, array(
            new MockIngredient('Chicken', 4, Units::OUNCE),
            new MockIngredient('thyme', 2, Units::TEASPOON)
        ));

        $expectedRecipe1Price = 4.0;
        $expectedRecipe2Price = 4.15;
        $expectedRecipe3Price = 1.1;

        $recipes = array($recipe1, $recipe2, $recipe3);

        $ingredientPricer = new MockIngredientPricer();
        $converter = new MockMeasurementConverter();

        $ranker = new RecipeRanker($ingredientPricer, $converter);

        $rankedRecipes = $ranker->rankRecipes($recipes);

        //print_r($rankedRecipes);
        $this->assertCount(3, $rankedRecipes);

        // Order should be Recipe 3, Recipe1, Recipe2

        $this->assertEquals($recipe1->getName(), $rankedRecipes[1]->getName());
        $this->assertEquals($recipe2->getName(), $rankedRecipes[2]->getName());
        $this->assertEquals($recipe3->getName(), $rankedRecipes[0]->getName());


        $this->assertEquals($expectedRecipe1Price / 2.0, $rankedRecipes[1]->getCostPerPersonFed());
        $this->assertEquals($expectedRecipe2Price / 2.0, $rankedRecipes[2]->getCostPerPersonFed());
        $this->assertEquals($expectedRecipe3Price / 1.0, $rankedRecipes[0]->getCostPerPersonFed());

        $this->assertEquals($expectedRecipe1Price, $rankedRecipes[1]->getTotalCost());
        $this->assertEquals($expectedRecipe2Price, $rankedRecipes[2]->getTotalCost());
        $this->assertEquals($expectedRecipe3Price, $rankedRecipes[0]->getTotalCost());

        $this->assertEquals(1, $rankedRecipes[0]->getRank());
        $this->assertEquals(2, $rankedRecipes[1]->getRank());
        $this->assertEquals(3, $rankedRecipes[2]->getRank());


        // Test with offset and count
        $rankedRecipes = $ranker->rankRecipes($recipes, null, 1, 1);
        $this->assertCount(1, $rankedRecipes);

        $this->assertEquals($recipe1->getName(), $rankedRecipes[0]->getName());
        $this->assertEquals(2, $rankedRecipes[0]->getRank());


    }
}