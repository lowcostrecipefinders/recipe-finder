<?php

namespace RecipeFinders\Test\RecipeRanker;

use RecipeFinders\Measurements\Units;
use RecipeFinders\Model\IngredientPriceInterface;
use RecipeFinders\Model\IngredientPricerInterface;
use RecipeFinders\Model\LocationInterface;
use RecipeFinders\Test\RecipeRanker\Model\MockIngredientPrice;

class MockIngredientPricer implements IngredientPricerInterface
{



    /**
     * Takes a list of ingredient names and returns pricing data for them
     *
     * @param  string[] $ingredientList A list of ingredient names
     *                                            TODO: Implementers, is this what you need, or will
     *                                                  you need something else?
     * @param  LocationInterface $location The user's location to get location specific pricing data
     *                                            TODO: Should this be required??  Implementers decide
     *
     * @return IngredientPriceInterface[] An associative array where the key is the ingredient name
     *                                    and the value is an IngredientPriceInterface object.
     */
    public function getPriceForIngredients(array $ingredientList, LocationInterface $location = null)
    {
        return array(
            'chicken' => new MockIngredientPrice(4.0, Units::POUND),
            'thyme' => new MockIngredientPrice(0.05, Units::TEASPOON),
        );
    }
}