<?php

namespace RecipeFinders\Test\RecipeRanker;

use RecipeFinders\Measurements\Converter;
use RecipeFinders\Measurements\Units;

class MockMeasurementConverter extends Converter
{
    public function convert($fromUnitOfMeasurement, $fromQuantity, $toUnitOfMeasurement)
    {
        if ($fromUnitOfMeasurement === $toUnitOfMeasurement) {
            return $fromQuantity;
        }

        switch($fromUnitOfMeasurement) {
            case Units::POUND:
                if ($toUnitOfMeasurement === Units::OUNCE) {
                    return 16.0 * $fromQuantity;
                }
                break;
            case Units::OUNCE:
                if ($toUnitOfMeasurement === Units::POUND) {
                    return $fromQuantity / 16.0;
                }
                break;
            case Units::TABLESPOON:
                if ($toUnitOfMeasurement === Units::TEASPOON) {
                    return 3.0 * $fromQuantity;
                }
                break;
            case Units::TEASPOON:
                if ($toUnitOfMeasurement === Units::TABLESPOON) {
                    return $fromQuantity / 3.0;
                }
                break;

        }

        return $fromQuantity;
    }
}