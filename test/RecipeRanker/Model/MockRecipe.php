<?php

namespace RecipeFinders\Test\RecipeRanker\Model;

use RecipeFinders\Model\IngredientInterface;
use RecipeFinders\Model\RecipeInterface;

class MockRecipe implements RecipeInterface
{

    public $ingredients = array();

    public $name;

    public $numberOfPeopleFed;

    public $id;

    public function __construct($id, $name, $numberOfPeopleFed, array $ingredients)
    {
        $this->id = $id;
        $this->name = $name;
        $this->numberOfPeopleFed = $numberOfPeopleFed;
        $this->ingredients = $ingredients;
    }

    /**
     * Returns an array of the ingredients needed for the recipe
     *
     * @return IngredientInterface[]
     */
    public function getIngredients()
    {
        return $this->ingredients;
    }

    /**
     * Returns the name of the recipe
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns how many people are supposed to be fed by this recipe
     *
     * I believe this is important for ranking.  A recipe that costs
     * $2.00, but only feeds one person should be ranked lower than
     * a recipe that costs $6.00 but feeds 8.
     *
     * If this data isn't available, we will need to improvise.
     *
     * @return int
     */
    public function getNumberOfPeopleFed()
    {
        return $this->numberOfPeopleFed;
    }

    /**
     * Returns a unique identifier for the recipe that can be used to find it again
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}