<?php

namespace RecipeFinders\Test\RecipeRanker;

use PHPUnit_Framework_TestCase as Test;
use RecipeFinders\Measurements\Units;
use RecipeFinders\RecipeRanker\Model\PricedIngredient;
use RecipeFinders\RecipeRanker\Model\RankedRecipe;
use RecipeFinders\Test\RecipeRanker\Model\MockIngredient;
use RecipeFinders\Test\RecipeRanker\Model\MockRecipe;

class RankedRecipeTest extends Test
{
    public function testRecipeRanker()
    {
        $ingredients = array(
            new PricedIngredient(new MockIngredient('Chicken', 2, Units::POUND), 3.10),
            new PricedIngredient(new MockIngredient('Thyme', 1, Units::TABLESPOON), 0.05),
            new PricedIngredient(new MockIngredient('Cranberries', 16, Units::OUNCE), 0.10),
        );

        $recipeName = 'Cranberry Time Chicken';
        $recipeId = "187985";
        $peopleFed = 4;

        $totalCost = 3.10 * 2 + 0.05 + 1.6;

        $mockRecipe = new MockRecipe($recipeId, $recipeName, $peopleFed, $ingredients);

        $rankedRecipe = RankedRecipe::newFromRecipe($mockRecipe, $ingredients);

        $this->assertInstanceOf("RecipeFinders\\Model\\RankedRecipeInterface", $rankedRecipe);
        $this->assertEquals($ingredients, $rankedRecipe->getIngredients());
        $this->assertEquals($recipeName, $rankedRecipe->getName());
        $this->assertEquals($recipeId, $rankedRecipe->getId());
        $this->assertEquals($peopleFed, $rankedRecipe->getNumberOfPeopleFed());
        $this->assertEquals($totalCost, $rankedRecipe->getTotalCost());
        $this->assertEquals($totalCost / $peopleFed, $rankedRecipe->getCostPerPersonFed());
        $this->assertEquals($rankedRecipe->getCostPerPersonFed(), $rankedRecipe->getRank());
    }
}