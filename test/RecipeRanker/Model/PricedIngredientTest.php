<?php

namespace RecipeFinders\Test\Model\RecipeRanker;

use PHPUnit_Framework_TestCase as Test;
use RecipeFinders\Measurements\Units;
use RecipeFinders\RecipeRanker\Model\PricedIngredient;
use RecipeFinders\Test\RecipeRanker\Model\MockIngredient;

class PricedIngredientTest extends Test
{
    public function testPricedIngredient()
    {
        $qty = 2;
        $uom = Units::CUP;
        $name = 'butter';
        $price = 0.6;

        $baseIngredient = new MockIngredient($name, $qty, $uom);

        $ingredient = new PricedIngredient($baseIngredient, $price);

        $this->assertEquals($qty, $ingredient->getQuantity());
        $this->assertEquals($name, $ingredient->getName());
        $this->assertEquals($uom, $ingredient->getUnitOfMeasurement());
        $this->assertEquals($price, $ingredient->getCostPerUnit());
        $this->assertEquals(1.2, $ingredient->getTotalCost());

        $ingredient->setCostPerUnit(0.4);
        $this->assertEquals(0.4, $ingredient->getCostPerUnit());
        $this->assertEquals(0.8, $ingredient->getTotalCost());

    }
}