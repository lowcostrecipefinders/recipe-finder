<?php

namespace RecipeFinders\Test\RecipeRanker\Model;

use RecipeFinders\Model\IngredientInterface;

class MockIngredient implements IngredientInterface
{

    public $name;
    public $quantity;
    public $unitOfMeasurement;

    public function __construct($name, $quantity, $unitOfMeasurement)
    {
        $this->name = $name;
        $this->quantity = $quantity;
        $this->unitOfMeasurement = $unitOfMeasurement;
    }

    /**
     * Returns the name of the ingredient
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the quantity of the ingredient used in the recipe in the proper unit of measurement
     *
     * @return int|float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Returns the unit of measurement
     *
     * @return string    The units of measurement should be defined as constants somewhere
     */
    public function getUnitOfMeasurement()
    {
        return $this->unitOfMeasurement;
    }

}
