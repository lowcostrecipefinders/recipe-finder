<?php

namespace RecipeFinders\Test\RecipeRanker\Model;

use RecipeFinders\Model\IngredientPriceInterface;

class MockIngredientPrice implements IngredientPriceInterface
{

    /**
     * @var
     */
    private $price;
    /**
     * @var
     */
    private $uom;

    public function __construct($price, $uom)
    {
        $this->price = $price;
        $this->uom = $uom;
    }

    /**
     * Returns the price for the ingredient per unit of measurement
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns the unit of measurement for the ingredient
     *
     * @return string  The unit of measurement.
     *                 These should be defined as constants somewhere to support conversion algorithms
     */
    public function getUnitOfMeasurement()
    {
        return $this->uom;
    }
}