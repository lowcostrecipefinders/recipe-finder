<?php

namespace RecipeFinders\Test\Measurements;


use RecipeFinders\Measurements\Converter;
use RecipeFinders\Measurements\Units;

class ConverterTest extends \PHPUnit_Framework_TestCase
{
    public function testConverter()
    {
        $converter = new Converter();

        $this->assertEquals(0, $converter->convert(Units::QUART, 0, Units::QUART));
        $this->assertEquals(1.0, $converter->convert(Units::QUART, 1.0, Units::QUART));

        $this->assertEquals(4.0, $converter->convert(Units::GALLON, 1, Units::QUART));

        $this->assertEquals(1000.0, $converter->convert(Units::LITER, 1, Units::GRAM));

        $this->assertEquals(3.0, $converter->convert(Units::KILOGRAM, 3, Units::LITER));

        $this->assertEquals(Converter::PIECE_WEIGHT * 3.0, $converter->convert(Units::PIECE, 3, Units::GRAM));

        $this->assertEquals(Converter::INCH_WEIGHT, $converter->convert(Units::INCH, 1, Units::GRAM));
    }
}
 