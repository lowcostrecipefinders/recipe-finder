<?php

namespace RecipeFinders\Test\Measurements;

use PHPUnit_Framework_TestCase as Test;
use RecipeFinders\Measurements\Units;

class UnitsTest extends Test
{
    public function testDetectUnitFromString()
    {
        $this->assertEquals(Units::CUP, Units::detectUnitFromString('cups'));
        $this->assertEquals(Units::CUP, Units::detectUnitFromString('cup'));
        $this->assertEquals(Units::CUP, Units::detectUnitFromString('3 cups of water'));

        $this->assertEquals(Units::FLUID_OUNCES, Units::detectUnitFromString('fluid ounces'));
        $this->assertEquals(Units::FLUID_OUNCES, Units::detectUnitFromString('fl oz'));
        $this->assertEquals(Units::FLUID_OUNCES, Units::detectUnitFromString('fl ozs'));
        $this->assertEquals(Units::FLUID_OUNCES, Units::detectUnitFromString('12 fluid ounces'));
        $this->assertEquals(Units::FLUID_OUNCES, Units::detectUnitFromString('12 fluid    ounces'));

        $this->assertEquals(Units::OUNCE, Units::detectUnitFromString('oz'));
        $this->assertEquals(Units::OUNCE, Units::detectUnitFromString('ounces'));

        $this->assertEquals(Units::PIECE, Units::detectUnitFromString('chicken breasts'));
        $this->assertEquals(Units::PIECE, Units::detectUnitFromString('gobbldly gook'));

        $this->assertEquals(Units::WHOLE, Units::detectUnitFromString('whole chicken'));


        $this->assertEquals(Units::CUP, Units::detectUnitFromString('2 cups diced cooked turkey'));
        $this->assertEquals(Units::PIECE, Units::detectUnitFromString('2 celery ribs, diced'));
        $this->assertEquals(Units::TEASPOON, Units::detectUnitFromString('1/2 teaspoon salt'));
        $this->assertEquals(Units::PIECE, Units::detectUnitFromString('6 hamburger buns, split'));


    }
}