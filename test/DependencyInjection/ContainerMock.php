<?php

namespace RecipeFinders\Test\DependencyInjection;

use RecipeFinders\DependencyInjection\Container;

class ContainerMock extends Container
{
    /**
     * Adds an object to the shared array which is used for resolving method dependencies
     *
     * @param object $object The object to be shared
     * @param string $alias  An alias to easily retrieve this object from the container
     */
    public function share( $object, $alias = null )
    {
        $key = get_class( $object );
        // For mock objects
        if (strpos($key, 'Mock') !== false) {
            $key = get_parent_class($key);
        }
        $this->shared[$key] = $object;
        if ( $alias != null ) {
            $this->shared[$alias] = $object;
        }


    }
}